ANALOG
Ch 1 Scale 100mV/, Pos -5.00mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 10.000 : 1, Skew 0.0s
Ch 2 Scale 20.0V/, Pos 39.0000V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 10.000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Edge, Source Ch 2, Slope Rising, Level -200.0mV

HORIZONTAL
Mode Normal, Ref Center, Main Scale 10.00us/, Main Delay 0.0s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

