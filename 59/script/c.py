from labtools import *

t, U = np.loadtxt(lt.file('data/c_a'), unpack=True)
t *= 1e6

U_max, U_min = lt.peakdetect(U, t, lookahead=5, delta=1)
t_max, U_max = np.array(U_max).T
t_min, U_min = np.array(U_min).T

def cos(t, a, b, c, d):
    return  a * np.cos(b * (t + c)) + d

a, b, c, d = lt.curve_fit(cos, t_max, U_max, p0=(10, np.pi / 10, 0, 10))

e = lt.mean(U_min)

m_a = a / (d - e)

f, V = np.loadtxt(lt.file('data/c_f'), unpack=True)
f *= 1e-3

V_max, _ = lt.peakdetect(V, f, lookahead=10)
f_max, V_max = np.array(V_max).T
f_max = f_max[V_max > 0.4]
V_max = V_max[V_max > 0.4]

m_f = lt.mean([V_max[0], V_max[2]]) * 2 / V_max[1]

tab = lt.SymbolRowTable()
tab.add_si_row('a', a, r'\volt')
tab.add_si_row('b', b, r'\per\micro\second')
tab.add_si_row('c', c, r'\micro\second')
tab.add_si_row('d', d, r'\volt')
tab.add_si_row(r'\mean{U_{\t{min}}}', e, r'\volt')
tab.add_hrule()
tab.add_si_row(r'V_{\t{max, M}}', V_max[::2], r'\volt', places=3)
tab.add_si_row(r'V_{\t{max, T}}', V_max[1], r'\volt', places=3)
tab.add_si_row(r'm_{\t{Ampl}}', m_a)
tab.add_si_row(r'm_{\t{Freq}}', m_f)
tab.savetable(lt.new('table/c.tex'))

x = np.linspace(t[0], t[-1], 10000)
fig = plt.figure()
fig.set_figheight(4)
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, U, 'g-', label="$U_3$")
ax.plot(x, cos(x, *lt.vals((a, b, c, d))), 'b-', label="Ausgleichskurve")
ax.plot(t_max, U_max, 'rx', label=r"$U_{\t{max}}$")
ax.plot(t_min, U_min, 'cx', label=r"$U_{\t{min}}$")
ax.set_ylim(-20, 40)
ax.set_xlabel(r'$\silabel{t}{\micro\second}$')
ax.set_ylabel(r'$\silabel{U}{\volt}$')
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
ax.legend(loc='lower center')
fig.savefig(lt.new('graphic/amp_ampl-mod_gen.pdf'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(f, V, 'b-')
ax.plot(f_max[::2], V_max[::2], 'rx', label=r"$V_{\t{max}, \t{M}}$")
ax.plot(f_max[1], V_max[1], 'gx', label=r"$V_{\t{max}, \t{T}}$")
ax.set_xlabel(r'$\silabel{f}{\kilo\hertz}$')
ax.set_ylabel(r'$\silabel{V}{\volt}$')
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(1))
ax.legend(loc='upper right')
fig.savefig(lt.new('graphic/freq_ampl-mod_gen.pdf'))
