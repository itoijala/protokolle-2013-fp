from labtools import *

t, U_1, U_2 = np.loadtxt(lt.file('data/f'), unpack=True)
t *= 1e6

fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)
ax.plot(t, U_1, 'b-')
ax.set_ylabel(r'$\silabel{U}{\volt}$')
ax.xaxis.set_major_formatter(mpl.ticker.NullFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(1))

ax2 = fig.add_subplot(2, 1, 2)
ax2.plot(t, U_2, 'g-')
ax2.set_xlabel(r'$\silabel{t}{\micro\second}$')
ax2.set_ylabel(r'$\silabel{U_{\t{M}}}{\volt}$')
ax2.xaxis.set_major_formatter(lt.SiFormatter())
ax2.yaxis.set_major_formatter(lt.SiFormatter(1))
fig.savefig(lt.new('graphic/amp_dem.pdf'))
