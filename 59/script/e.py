from labtools import *

Δt, U = np.loadtxt(lt.file('data/proportionality'), unpack=True, skiprows=3)
U *= 1e-3

def cos(Δt, A, B, C, D):
    return A * np.cos(B * (Δt + C)) + D

A, B, C, D = lt.curve_fit(cos, Δt, U, p0=[0.3, 2 * np.pi / 1000, 0, 0])

tab = lt.SymbolColumnTable()
tab.add_si_column('Δt', Δt[:len(Δt) / 2 + 1], r'\nano\second', places=0)
tab.add_si_column('U', U[:len(Δt) / 2 + 1] * 1e3, r'\milli\volt', places=0)
tab.add_column(lt.VerticalSpace())
tab.add_si_column('Δt', Δt[len(Δt) / 2 + 1:], r'\nano\second', places=0)
tab.add_si_column('U', U[len(Δt) / 2 + 1:] * 1e3, r'\milli\volt', places=0)
tab.savetable(lt.new('table/propToCosM.tex'))

tab2 = lt.SymbolRowTable()
tab2.add_si_row('A', A, r'\volt')
tab2.add_si_row('B', B, r'\per\nano\second')
tab2.add_si_row('C', C, r'\nano\second')
tab2.add_si_row('D', D, r'\volt')
tab2.savetable(lt.new('table/propToCosF.tex'))

x = np.linspace(0, 450, 10000)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, cos(x, *lt.vals((A, B, C, D))) * 1e3, 'b-', label='Ausgleichskurve')
ax.plot(Δt, U * 1e3, 'rx', label='Messwerte')
ax.set_xlabel(r'$\silabel{\Del{t}}{\nano\second}$')
ax.set_ylabel(r'$\silabel{U}{\milli\volt}$')
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
lt.ax_reverse_legend(ax, 'upper right')
fig.savefig(lt.new('graphic/propCos.pdf'))
