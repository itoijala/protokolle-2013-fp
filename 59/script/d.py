from labtools import *

f, U = np.loadtxt(lt.file('data/d'), unpack=True)
f *= 1e-3

f_max, _ = lt.peakdetect(U, f, lookahead=20)
f_max, U_max = np.array(f_max).T
f_T = f_max[3]

t1, t2 = np.loadtxt(lt.file('data/zero-crossing'), unpack=True)

freqH = lt.mean(2 * np.pi * (t2 - t1) / (t1 * t2)) * 1e6
m = freqH / (2 * np.pi * f_T)

tab = lt.SymbolColumnTable()
tab.add_si_column('t_1', t1, r'\nano\second', places=0)
tab.add_si_column('t_2', t2, r'\nano\second', places=0)

tab2 = lt.SymbolRowTable()
tab2.add_si_row(r'f_{\t{T}}', f_T, r'\kilo\hertz', places=0)
tab2.add_si_row(r'\mean{ω_{\Del{}}}', freqH * 1e-3, r'\mega\hertz')
tab2.add_si_row('m', m)

tab3 = lt.Table()
tab3.add(lt.Row())
tab3.add_hrule()
tab3.add(lt.Row())
tab3.add(lt.Column([tab, tab2], "@{}c@{}"))
tab3.savetable(lt.new('table/zero-crossing.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(f, U, 'b-')
ax.plot(f_max[3], U_max[3], 'gx', label=r"$f_{\t{T}}$")
ax.plot(f_max[[2, 4]], U_max[[2, 4]], 'rx', label=r"$f_{\t{T}} ± f_{\t{M}}$")
ax.set_xlabel(r'$\silabel{f}{\kilo\hertz}$')
ax.set_ylabel(r'$\silabel{V}{\volt}$')
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
ax.legend(loc='upper right')
fig.savefig(lt.new('graphic/phase_var_freq.pdf'))
