from labtools import *
from minuit2 import Minuit2

t, U = np.loadtxt(lt.file('data/a'), unpack=True)
t *= 1e6
U *= 1e3

f, V = np.loadtxt(lt.file('data/b'), unpack=True)
f *= 1e-3
V *= 1e3

x = np.linspace(t[0], t[-1], 10000)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, U, 'b-')
ax.set_xlabel(r'$\silabel{t}{\micro\second}$')
ax.set_ylabel(r'$\silabel{U_{\t{R}}}{\milli\volt}$')
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
fig.savefig(lt.new('graphic/amp_ampl-mod.pdf'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(f, V, 'b-')
ax.set_xlabel(r'$\silabel{f}{\kilo\hertz}$')
ax.set_ylabel(r'$\silabel{V}{\milli\volt}$')
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
fig.savefig(lt.new('graphic/freq_ampl-mod.pdf'))
