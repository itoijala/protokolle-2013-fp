from labtools import *

t, U_1 = np.loadtxt(lt.file('data/h1'), unpack=True)
t *= 1e6

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, U_1, 'b-')
ax.set_xlabel(r'$\silabel{t}{\micro\second}$')
ax.set_ylabel(r'$\silabel{U}{\volt}$')
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
fig.savefig(lt.new('graphic/freq_dem_amp.pdf'))

t, U_2, U_3 = np.loadtxt(lt.file('data/h2'), unpack=True)
t *= 1e6
U_2 *= 1e3

fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)
ax.plot(t, U_2, 'b-')
ax.set_ylabel(r'$\silabel{U}{\milli\volt}$')
ax.xaxis.set_major_formatter(mpl.ticker.NullFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())

ax2 = fig.add_subplot(2, 1, 2)
ax2.plot(t, U_3, 'g-')
ax2.set_xlabel(r'$\silabel{t}{\micro\second}$')
ax2.set_ylabel(r'$\silabel{U_{\t{M}}}{\volt}$')
ax2.xaxis.set_major_formatter(lt.SiFormatter())
ax2.yaxis.set_major_formatter(lt.SiFormatter())
fig.savefig(lt.new('graphic/freq_dem_LP.pdf'))
