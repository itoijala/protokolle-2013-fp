\input{header/report.tex}

\SetExperimentNumber{59}

\begin{document}
  \maketitlepage{Modulation und Demodulation elektrischer Schwingungen}{06.05.2013}{05.06.2013}
  \section{Einleitung}
    Ziel des Versuchs ist die Untersuchung von Amplituden- und Frequenzmodulation und -demodulation.

    Um mittels einer elektromagnetischen Welle Informationen zu transportieren, muss die Information, die in der zeitabhängigen Spannung $\f{U_{\t{M}}}(t)$ kodiert ist, mit einer Trägerwelle $\f{U_{\t{T}}}(t)$ der Frequenz $ω_{\t{T}}$ moduliert werden.
    Ohne Beschränkung der Allgemeinheit wird hier angenommen, dass $U_{\t{M}}$ aus nur einer Frequenz $ω_{\t{M}}$ besteht.
    Daher soll gelten
    \begin{eqn}
      \f{U_{\g\{\t{T}, \t{M}\}}}(t) = \hat{U}_{\g\{\t{T}, \t{M}\}} \cos.ω_{\g\{\t{T}, \t{M}\}} t. ,
    \end{eqn}
    wobei $\hat{U}_{\g\{\t{T}, \t{M}\}}$ die Amplitude des Träger- beziehungsweise des Modulationssignals ist.
    Dabei werden hauptsächlich zwei unterschiedliche Methoden der Modulation verwendet, die sich in ihren Vor- und Nachteilen unterscheiden: Amplituden- und Frequenzmodulation.
  \section{Amplitudenmodulation}
    \subsection{Theorie}
      Bei der Amplitudenmodulation wird die Amplitude des Trägersignals als Funktion des Modulationssignals variiert.
      Für das modulierte Signal gilt
      \begin{eqns}
        \f{U_3}(t) &=& \hat{U}_{\t{T}} \g(1 + m \cos.ω_{\t{M}} t.) \cos.ω_{\t{T}} t. \\
                   &=& \hat{U}_{\t{T}} \g(\cos.ω_{\t{T}} t. + \frac{m}{2} \cos.\g(ω_{\t{T}} + ω_{\t{M}}) t. + \frac{m}{2} \cos.\g(ω_{\t{T}} - ω_{\t{M}}) t.) , \eqlabel{eqn:ampl-theory}
      \end{eqns}
      wobei $m \in \intoc{0}{1}$ der Modulationsgrad ist.
      Die Amplitude des modulierten Signals, das aus den drei Frequenzen $ω_{\t{T}}$ und $ω_{\t{T}} ± ω_{\t{M}}$ besteht, variiert zwischen den Werten $\hat{U}_{\t{T}} \g(1 ± m)$.
      Diese Sachverhalte werden durch die Abbildungen \ref{fig:amplitude-amplitude} und \ref{fig:amplitude-frequenz} veranschaulicht.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/amplitude-amplitude.pdf}
        \caption{Ein amplitudenmoduliertes Signal \cite{anleitung59}.}
        \label{fig:amplitude-amplitude}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/amplitude-frequenz.pdf}
        \caption{Frequenzspektrum eines amplitudenmodulierten Signals \cite{anleitung59}.}
        \label{fig:amplitude-frequenz}
      \end{figure}
      In dem Fall, dass das Modulationssignal aus mehreren Frequenzen besteht, werden aus den Frequenzlinien $ω_{\t{T}} ± ω_{\t{M}}$ Frequenzbänder.
      Das Verfahren kann optimiert werden, indem die Trägerfrequenz, die keine Informationen enthält, und eines der Frequenzbänder, das dieselben Informationen wie das andere enthält, nicht übertragen werden.
      Ersteres wird Trägerabstrahlungsunterdrückung und zweites Einseitenbandmodulation genannt.
      Nachteile dieses Verfahrens sind die geringe Störsicherheit und Verzerrungsfreiheit.
      \FloatBarrier
    \subsection{Amplitudenmodulation ohne Trägerunterdrückung}
      \subsubsection{Aufbau und Durchführung}
        Die einfachste Amplitudenmodulationsschaltung besteht aus einer Diode und ist in Abbildung \ref{fig:c} abgebildet.
        Diese Schaltung kann verwendet werden, da die Diode eine nichtlineare Kennlinie
        \begin{eqn}
          \f{I}(U) = \sum a_i U^i
        \end{eqn}
        besitzt, woraus
        \begin{eqn}
          \f{I}(U_{\t{T}} + U_{\t{M}}) = a_0 + a_1 \g(U_{\t{T}} + U_{\t{M}}) + a_2 \g(U_{\t{T}} + U_{\t{M}}) + 2 a_2 U_{\t{T}} U_{\t{T}} + \cdots
        \end{eqn}
        folgt, worin der gewünschte Term $U_{\t{T}} U_{\t{M}}$ enthalten ist.
        Ein Effekt aller Terme der Taylorentwicklung mit der Ordnung $\LandauO(U^2)$ sind auftretende Oberwellen.
        Die anderen Terme enthalten nicht die gewünschte Information, können aber, da sie außerhalb des Frequenzbereichs $\intcc{ω_{\t{T}} - ω_{\t{M}}}{ω_{\t{T}} + ω_{\t{M}}}$ liegen, mit einem Bandfilter abgeschnitten werden, was aber zu Effizienzverlusten führt.
        Das modulierte Signal wird mit einem Speicheroszilloskop und mit einem Frequenzanalysator aufgenommen.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/c.pdf}
          \caption{Schaltung zur Untersuchung der Amplitudenmodulation ohne Trägerunterdrückung \cite{anleitung59}.}
          \label{fig:c}
        \end{figure}
      \subsubsection{Auswertung}
        Der Spannungsverlauf des amplitudenmodulierten Signals ist in Abbildung \ref{fig:beat_ampl-mod_gen} und sein Frequenzspektrum in Abbildung \ref{fig:freq-spect_ampl-mod_gen} dargestellt.
        Um den Modulationsgrad $m$ mit Hilfe von \eqref{eqn:ampl-theory} zu ermitteln, wird eine Ausgleichsrechnung mit den Maxima der Spannung und der Funktion
        \begin{eqn}
          U_{\t{max}} = a \cos.b \g(t_{\t{max}} + c). + d
        \end{eqn}
        durchgeführt.
        Der Modulationsgrad ergibt sich aus
        \begin{eqn}
          m_{\t{Ampl}} = \frac{a}{d - \mean{U_{\t{min}}}}
        \end{eqn}
        und kann ebenfalls mittels
        \begin{eqn}
          m_{\t{Freq}} = 2 \frac{\mean{V_{\t{max}, \t{M}}}}{V_{\t{max}, \t{T}}}
        \end{eqn}
        aus dem Frequenzspektrum ermittelt werden.
        Die Punkte $V_{\t{max}}$ und $V_{\t{min}}$, die in Abbildung \ref{fig:freq-spect_ampl-mod_gen} eingezeichnet sind, der Modulationsgrad sowie die Ergebnisse der Ausgleichsrechnung stehen in Tabelle \ref{tab:c}.
        \begin{table}
          \caption{Parameter der nichtlinearen Ausgleichsrechnung.}
          \label{tab:c}
          \input{table/c.tex}
        \end{table}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/amp_ampl-mod_gen.pdf}
          \caption{Amplitudenmoduliertes Signal ohne Trägerunterdrückung.}
          \label{fig:beat_ampl-mod_gen}
        \end{figure}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/freq_ampl-mod_gen.pdf}
          \caption{Frequenzspektrum eines amplitudenmodulierten Signals ohne Trägerunterdrückung.}
          \label{fig:freq-spect_ampl-mod_gen}
        \end{figure}
      \FloatBarrier
    \subsection{Amplitudenmodulation mit Trägerunterdrückung}
      \subsubsection{Aufbau und Durchführung}
        Ein Ringmodulator, abgebildet in Abbildung \ref{fig:ringmodulator}, kann zur Amplitudenmodulation mit Trägerunterdrückung verwendet werden.
        Der Ringmodulator besteht aus vier Dioden, die zu einem Ring geschaltet werden.
        Falls keine Modulationsspannung anliegt, liegt auch zwischen den Punkten $\mathup{α}$ und $\mathup{β}$ keine Spannung an.
        Wird eine Modulationsspannung $U_{\t{M}}$ angelegt, ändert sich die Ausgangsspannung $U_{\t{R}}$ mit der Frequenz von $U_{\t{M}}$.
        Bei einem idealen Aufbau gilt
        \begin{eqn}
          \f{U_{\t{R}}}(t) \propto \f{U_{\t{T}}}(t) \f{U_{\t{M}}}(t)
        \end{eqn}
        und die Trägerspannung wird unterdrückt.
        Die Ausgangsspannung wird mit einem Speicheroszilloskop und einem Frequenzanalysator untersucht.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/ringmodulator.pdf}
          \caption{Schaltbild eines Ringmodulators \cite{anleitung59}.}
          \label{fig:ringmodulator}
        \end{figure}
      \subsubsection{Auswertung}
        Die Ausgangsspannung ist in Abbildung \ref{fig:beat_ampl-mod} und ihr Frequenzspektrum in Abbildung \ref{fig:freq-spect_ampl-mod} aufgezeichnet.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/amp_ampl-mod.pdf}
          \caption{Schwebung bei amplitudenmodulierter Schwingung mit Trägerunterdrückung.}
          \label{fig:beat_ampl-mod}
        \end{figure}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/freq_ampl-mod.pdf}
          \caption{Frequenzspektrum bei amplitudenmodulierter Schwingung mit Trägerunterdrückung.}
          \label{fig:freq-spect_ampl-mod}
        \end{figure}
      \FloatBarrier
    \subsection{Amplitudendemodulation mit einem Ringmodulator}
      \subsubsection{Aufbau und Durchführung}
        Ein Ringmodulator kann auch zur Demodulation von amplitudenmodulierten Signalen gemäß der Schaltung in Abbildung \ref{fig:ring-demodulation} verwendet werden.
        Da der Ringmodulator das Produkt der Eingangsspannungen bildet, treten am Ausgang die Frequenzen $ω_{\t{M}}$ und $2 ω_{\t{T}} ± ω_{\t{M}}$ auf, wobei letztere durch einen Tiefpass entfernt werden können.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/ring-demodulation.pdf}
          \caption{Aufbau zur Amplitudendemodulation mittels eines Ringmodulators \cite{anleitung59}.}
          \label{fig:ring-demodulation}
        \end{figure}

        Für die Demodulation mit einem Ringmodulator wird eine Trägerspannung benötigt, die phasengleich zu der bei der Modulation verwendeten Trägerspannung ist.
        Gibt es eine Phasendifferenz $φ$ zwischen den Trägerspannungen, so entsteht am Ausgang zusätzlich eine Gleichspannung, die proportional zu $\cos.φ.$ ist.
        Zur Messung dieses Effekts wird die Schaltung aus Abbildung \ref{fig:phase} verwendet.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/phase.pdf}
          \caption{Aufbau zur Messung der Abhängigkeit der Ausgangsspannung von der Phase \cite{anleitung59}.}
          \label{fig:phase}
        \end{figure}
      \subsubsection{Messwerte und Auswertung}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/amp_dem.pdf}
          \caption{Demodulierte Amplitude nach Amplitudenmodulation und Amplitude vor Modulation.}
          \label{fig:amp_dem}
        \end{figure}

        Die Messwerte sind in Abbildung \ref{fig:propCos} aufgetragen.
        Durch die Punkte wurde die Funktion
        \begin{eqn}
          A \cos.B \g(\Del{t} + C). + D
        \end{eqn}
        mit den Parametern $A$, $B$, $C$ und $D$ gelegt, deren ermittelten Zahlenwerte in Tabelle \ref{tab:propCosF} aufgelistet sind.
        Die Funktion ist ebenfalls in Abbildung \ref{fig:phase_variation_freq} aufgetragen.
        Wegen experimentellen Einschränkungen bei den Verzögerungsleitungen, die die Phasenverschiebung hervorgerufen haben, war es nur möglich, Teile einer halben Periode der Cosinusfunktion zu messen.
        \begin{table}
          \caption{Messwerte aus der Proportionalitätsmessung.}
          \label{tab:propCosM}
          \input{table/propToCosM.tex}
        \end{table}
        \begin{table}
          \caption{Ermittelte Parameter der Ausgleichsrechnung für Proportionalitätsmessung.}
          \label{tab:propCosF}
          \input{table/propToCosF.tex}
        \end{table}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/propCos.pdf}
          \caption{Messwerte der Proportionalitätsmessung und Ausgleichskurve.}
          \label{fig:propCos}
        \end{figure}
      \FloatBarrier
    \subsection{Amplitudendemodulation mit einer Diode}
      \subsubsection{Aufbau und Durchführung}
        Eine alternative Schaltung zur Amplitudendemodulation, abgebildet in Abbildung \ref{fig:diode-demodulation}, besteht aus einer Diode und einem Tiefpass.
        Die Diode schneidet alle negative Halbwellen des modulierten Signals ab und der Tiefpass entfernt alle Frequenzen, die Vielfache von der Trägerfrequenz sind.
        Vorteil dieser Methode ist der Verzicht auf die Trägerspannung bei der Demodulation.
        Nachteil ist eine Verzerrung verursacht durch die exponentielle Kennlinie der Diode, die aber für kleine Modulationsgrade beschränkt werden kann.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/diode-demodulation.pdf}
          \caption{Schaltung zur Amplitudendemodulation mit einer Diode \cite{anleitung59}.}
          \label{fig:diode-demodulation}
        \end{figure}
      \subsubsection{Auswertung}
        Die Ergebnisse der Demodulation mit einer Gleichrichterdiode sind analog zum letzten Kapitel in zwei Teilgraphen dargestellt.
        Die Amplitude am Punkt A, verglichen mit dem Modulationssignal, befindet sich in Abbildung \ref{fig:amp_dem_A}.
        Hinter dem Tiefpass ergibt sich das Bild aus Abbildung \ref{fig:amp_dem_LP}.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/amp_dem_A.pdf}
          \caption{Demodulierte Amplitude nach Amplitudenmodulation und Amplitude vor Modulation am Punkt A.}
          \label{fig:amp_dem_A}
        \end{figure}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/amp_dem_LP.pdf}
          \caption{Demodulierte Amplitude nach Amplitudenmodulation und Amplitude vor Modulation hinter dem Tiefpass.}
          \label{fig:amp_dem_LP}
        \end{figure}
  \section{Frequenzmodulation}
    \subsection{Theorie}
      Bei der Frequenzmodulation wird die Frequenz des Trägersignals als Funktion des Modulationssignals variiert.
      Für das modulierte Signal gilt
      \begin{eqns}
        \f{U}(t) &=& \hat{U} \cos(\int_0^t \dif{τ} \f{ω}(τ)) \\
                 &=& \hat{U} \cos(ω_{\t{T}} t + ω_{\Del{}} \int_0^t \dif{τ} \f{U_{\t{M}}}(τ)) ,
        \itext{wobei $ω_{\Del{}}$ der Frequenzhub ist.
        Mit $\f{U_{\t{M}}}(t) = \cos.ω_{\t{M}} t.$ folgt}
        \f{U}(t) &=& \hat{U} \cos(ω_{\t{T}} t + \frac{ω_{\Del{}}}{ω_{\t{M}}} \sin.ω_{\t{M}} t.) .
      \end{eqns}
      Daraus ergeben sich die Momentanfrequenz
      \begin{eqn}
        \f{ω}(t) = ω_{\t{T}} + ω_{\Del{}} \cos.ω_{\t{M}} t.
      \end{eqn}
      und der Modulationsindex
      \begin{eqn}
        m = \frac{ω_{\Del{}}}{ω_{\t{M}}} .
      \end{eqn}
      Das frequenzmodulierte Signal wird durch Abbildung \ref{fig:frequenz-amplitude} veranschaulicht.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/frequenz-amplitude.pdf}
        \caption{Ein frequenzmoduliertes Signal \cite{anleitung59}.}
        \label{fig:frequenz-amplitude}
      \end{figure}

      Im Folgenden soll nur die Schmalband-Frequenzmodulation, also $m \ll 1$ betrachtet werden.
      Mittels der Additionstheoreme folgt
      \begin{eqns}
        \f{U}(t) &=& \hat{U} \cos(ω_{\t{T}} t + m \sin.ω_{\t{M}} t.) \\
                 &=& \hat{U} \g(\cos.ω_{\t{T}} t. \cos(m \sin.ω_{\t{M}} t.) - \sin.ω_{\t{T}} t. \sin(m \sin.ω_{\t{M}} t.)) .
      \end{eqns}
      Durch Entwicklung in erster Ordnung in $m$, also mit
      \begin{eqns}[rCrCl]
        \sin.mx. &=& mx &+& \LandauO(m^2) \\
        \cos.mx. &=& 1  &+& \LandauO(m^2)
      \end{eqns}
      folgt
      \begin{eqns}
        \f{U}(t) &=& \hat{U} \g(\cos.ω_{\t{T}} t. - m \sin.ω_{\t{T}} t. \sin.ω_{\t{M}} t.) + \LandauO(m^2) \\
                 &=& \hat{U} \g(\cos.ω_{\t{T}} t. - \frac{m}{2} \sin.\g(ω_{\t{T}} - ω_{\t{M}}) t. - \frac{m}{2} \sin.\g(ω_{\t{T}} + ω_{\t{M}}) t.) + \LandauO(m^2) .
      \end{eqns}
      Das Frequenzspektrum des frequenzmodulierten Signals besteht also aus den drei Frequenzen $ω_{\t{T}}$ und $ω_{\t{T}} ± ω_{\t{M}}$, wobei die Trägerfrequenz um $\PI / 2$ gegenüber den anderen verschoben ist.
      \FloatBarrier
    \subsection{Frequenzmodulation}
      \subsubsection{Aufbau und Durchführung}
        Ein frequenzmoduliertes Signal kann mittels der Schaltung aus Abbildung \ref{fig:freq-modulation} erreicht werden.
        Dabei werden die Seitenbänder mit den Frequenzen $ω_{\t{T}} ± ω_{\t{M}}$ mittels eines Ringmodulators erzeugt, genau wie bei der Amplitudenmodulation.
        Die um $\PI / 2$ phasenverschobene Trägerspannung wird durch einen Phasenverschieber erreicht.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/freq-modulation.pdf}
          \caption{Schaltung zur Frequenzmodulation \cite{anleitung59}.}
          \label{fig:freq-modulation}
        \end{figure}
      \subsubsection{Messwerte und Auswertung}
        Bei der frequenzmodulierten Schwingung ist es nicht gelungen, die Zeitabhängigkeit der modulierten Schwingung unmittelbar auf dem Schirm des Oszillographen darzustellen.
        Die Verschmierung der Sinuskurve für $U_3$ ist jedoch in Abbildung \ref{fig:phase_variation_amp} gut erkennbar. Getriggert wurde auf die Trägerspannung, die im unteren Teil des Bildes zu sehen ist.
        Das Frequenzspektrum für diesen Fall findet sich in Abbildung \ref{fig:phase_variation_freq}.
        Für den Frequenzhub gilt
        \begin{eqn}
          ω_{\Del{}} = 2 \PI \frac{t_2 - t_1}{t_2 t_1} .
        \end{eqn}
        Somit folgt für den Modulationsindex
        \begin{eqn}
          m = \frac{\mean{ω_{\Del{}}}}{2 \PI f_{\t{T}}} .
        \end{eqn}
        Die Messwerte für $t_1$ und $t_2$ stehen mit den Ergebnissen für $ω_{\Del{}}$ und $m$ in Tabelle \ref{tab:zero-crossing}.
        Die Modulationsfrequenz wird dabei aus dem Frequenzspektrum entnommen.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/d.jpg}
          \caption{Periodische Phasenvariation von $U_3$ relativ zu $U_\t{T}$.}
          \label{fig:phase_variation_amp}
        \end{figure}
        \begin{table}
          \caption{Nulldurchgänge und Abstandsbetrag.}
          \label{tab:zero-crossing}
          \input{table/zero-crossing.tex}
        \end{table}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/phase_var_freq.pdf}
          \caption{Frequenzspektrum bei Phasenvariation.}
          \label{fig:phase_variation_freq}
        \end{figure}
      \FloatBarrier
    \subsection{Frequenzdemodulation}
      \subsubsection{Aufbau und Durchführung}
        Die Schaltung aus Abbildung \ref{fig:freq-demodulation} kann zur Demodulation frequenzmodulierter Signale verwendet werden.
        Dabei wird das frequenzmodulierte Signal durch den Schwingkreis in ein amplitudenmoduliertes übersetzt, welches dann mittels einer Diode demoduliert werden kann.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/freq-demodulation.pdf}
          \caption{Schaltung zur Frequenzdemodulation \cite{anleitung59}.}
          \label{fig:freq-demodulation}
        \end{figure}
      \subsubsection{Auswertung}
        Die Umwandlung in ein amplitudenmoduliertes Signal ist in Abbildung \ref{fig:freq_dem_amp} veranschaulicht.
        Hinter dem Tiefpass ergibt sich das Bild aus Abbildung \ref{fig:freq_dem_LP}.
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/freq_dem_amp.pdf}
          \caption{Umwandlung einer Frequenzmodulation zu einer Amplitudenmodulation nach dem Schwingkreis.}
          \label{fig:freq_dem_amp}
        \end{figure}
        \begin{figure}
          \includegraphics[width=\textwidth]{graphic/freq_dem_LP.pdf}
          \caption{Demodulierte Spannung nach dem Tiefpass und Modulationsspannung am Generator.}
          \label{fig:freq_dem_LP}
        \end{figure}
  \section{Diskussion}
    Der zeitabhängige Verlauf der Spannung in Abbildung \ref{fig:beat_ampl-mod_gen} zeigt gut die entstehende Schwebung.
    Auch das Frequenzspektrum in Abbildung \ref{fig:freq-spect_ampl-mod_gen} zeigt die erwarteten Frequenzen.
    Oberwellen von $ω_\t{T}$ sind, wie schon diskutiert, in diesem System zwar zu erwarten, jedoch erst ab $\SI{2}{\mega\hertz}$.
    Dieser Bereich wurde nicht mehr vermessen.
    Der Modulationsgrad, der aus den beiden Abbildungen ermittelt wurde, unterscheidet sich um einen Faktor 2.

    Bei den analogen Abbildungen \ref{fig:beat_ampl-mod} und \ref{fig:freq-spect_ampl-mod} tritt auch das erwartete Verhalten auf.
    Zwischen den zwei größeren Peaks im Frequenzspektrum liegt $ω_\t{T}$.
    Beim Vergleich mit dem Frequenzspektrum ohne Trägerunterdrückung fällt auf, dass die Peaks vermeintlich größer sind; dies ist jedoch lediglich ein Skalierungsunterschied.

    Wie in Abbildung \ref{fig:amp_dem} ersichtlich, klappt die Demodulation der Amplitudenmodulation mit dem Ringmodulator gut.
    Obwohl alle verfügbaren experimentellen Möglichkeiten bei der Proportionalitätsmessung, vergleiche Abbildung \ref{fig:propCos}, ausgeschöpft wurden, sind die Fehler der Ausgleichsrechnung, wie aus Tabelle \ref{tab:propCosF} ersichtlich, groß.
    Dies ist darauf zurückzuführen, dass die Messwerte lediglich ein Teil der Cosinusperiode abdecken.

    Wird die Amplitudenmodulation mit einer Diode demoduliert, so tritt eine Frequenzschiebung auf.
    Dies gilt sowohl vor (Abbildung \ref{fig:amp_dem_A}), als auch nach dem Tiefpass (Abbildung \ref{fig:amp_dem_LP}).
    Des Weiteren ist das Signal nach dem Tiefpass zusätzlich noch verrauscht.

    Die Frequenzmodulation funktionierte problemlos.
    Bei der Demodulation ergab sich zwar nach dem Schwingkreis aus der Frequenzmodulation eine Amplitudenmodulation, siehe Abbildung \ref{fig:freq_dem_amp}, jedoch war das Signal nach Demodulation dieser Amplitudenmodulation nicht mehr ersichtlich, wie in Abbildung \ref{fig:freq_dem_LP} zu sehen ist.
  \makebibliography
\end{document}
