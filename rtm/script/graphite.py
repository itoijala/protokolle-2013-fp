from labtools import *
from matplotlib.patheffects import Stroke
from stm import matrix_table, return_matrix, render_matrix
from minuit2 import Minuit2

S_inv = np.resize(unc.correlated_values(np.loadtxt(lt.file('result/S_inv')), np.loadtxt(lt.file('result/S_inv_error'))), (2, 2))

L = np.loadtxt(lt.file('data/L')) * 1e6
s = np.loadtxt(lt.file('data/s'))
v_scan = np.loadtxt(lt.file('data/v_scan'))

t = v_scan * 128

def circle(point):
    return plt.Circle(point, 6, fill=False, color='b')

def parallelogram(point):
    return plt.Polygon([point + (-12, 16), point + (0, 16), point + (12, -16), point + (0, -16)], fill=False, color='b')

def process(data, points, index1, index2, shape, figname, vname):
    x, y, z = np.loadtxt(data, unpack=True)
    z = np.reshape(z, (128, 128))

    x = x[:128]
    y = y[::128]
    Δ = x[1] - x[0]

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    for point in points:
        ax.add_patch(shape(point))
        ax.plot([point[0]], [point[1]], 'bx')

    c = []
    for index in [index1, index2[::-1]]:
        def chi2(x, y, u, v):
            s = 0
            for i, j in enumerate(index):
                s += np.sum((points[j] - np.array((x, y)) - i / (len(index) - 1) * np.array((u, v)))**2)
            return s

        minuit = Minuit2(chi2, x=points[index[0]][0], y=points[index[0]][1], u=(points[index[-1]][0] - points[index[0]][0]), v=(points[index[-1]][1] - points[index[0]][1]))
        minuit.migrad()
        c.append(Δ / (len(index) - 1) * np.array(unc.correlated_values([minuit.values["u"], minuit.values["v"]], np.array(minuit.matrix())[2:,2:])))

        for i in range(1, len(index)):
            arr = ax.arrow(minuit.values["x"] + (i - 1) / (len(index) - 1) * minuit.values["u"], minuit.values["y"] + (i - 1) / (len(index) - 1) * minuit.values["v"], 1 / (len(index) - 1) * minuit.values["u"], 1 / (len(index) - 1) * minuit.values["v"], length_includes_head=True, color='r', head_width=1.5)
            arr.set_path_effects([Stroke(joinstyle="miter", capstyle='butt')])

    open(vname, "w").write(" & ".join([
        ", ".join([
            r"$\g({})$".format(matrix_table(np.resize([lt.round_places(float(v3), 0) for v3 in points[i]], (2, 1)))) for i in index])
        + " & " + r"$\g({}) ± \g({})$".format(*return_matrix(np.resize(c1, (2, 1)))) for index, c1 in zip([index1, index2], c)]))

    im = ax.imshow(z, origin='lower', cmap=mpl.cm.afmhot, norm=mpl.colors.Normalize(-0.78125 / 8, 0.78125 / 8))
    ax.xaxis.set_major_formatter(lt.SiFormatter(3, factor=Δ))
    ax.yaxis.set_major_formatter(lt.SiFormatter(3, factor=Δ))
    ax.set_xlabel(r'$\silabel{x}{\nano\meter}$')
    ax.set_ylabel(r'$\silabel{y}{\nano\meter}$')
    cb = fig.colorbar(im, ax=ax, format=lt.SiFormatter(2))
    cb.set_label(r'\silabel{z}{\nano\meter}')
    fig.savefig(figname)

    return c

def process2(data_for, data_back, Cf, Cfe, Ff, Ffe):
    c1_for, c2_for = process(*data_for)
    c1_back, c2_back = process(*data_back)

    c1 = lt.mean(np.vstack((c1_for, c1_back)))
    c2 = lt.mean(np.vstack((c2_for, c2_back)))

    C = np.array([c1, c2])
    F = np.dot(C, S_inv.T)

    render_matrix(C.T, Cf, Cfe)
    render_matrix(F.T, Ff, Ffe)

    return C, F

C_up, F_up = process2((lt.file('data/graphite-up-for'),
                       np.array([          (49, 107), (75, 97), (98, 88), (121, 80),
                                 (19, 80), (48,  63), (74, 55), (98, 44),
                                 (22, 30), (47,  19)]),
                       [4, 5, 6, 7],
                       [1, 5, 8],
                       parallelogram, lt.new('graphic/up-for.pdf'), lt.new('result/graphit-up-for.tex')),
                      (lt.file('data/graphite-up-back'),
                       np.array([                    (80, 104), (107, 93),
                                 (30, 83), (55, 71), (80,  58), (106, 45),
                                 (32, 29), (59, 19)]),
                       [2, 3, 4, 5],
                       [0, 3, 6],
                       parallelogram, lt.new('graphic/up-back.pdf'), lt.new('result/graphit-up-back.tex')),
                      lt.new('result/C_up.tex'), lt.new('result/C_up_error.tex'),
                      lt.new('result/F_up.tex'), lt.new('result/F_up_error.tex'))

C_down, F_down = process2((lt.file('data/graphite-down-for'),
                           np.array([(27, 70), (48, 68), (70, 67), (89, 65), (109, 61),
                                     (25, 55), (46, 53), (67, 51), (87, 49), (105, 46),
                                     (23, 39), (45, 37),           (83, 32), (103, 30),
                                     (20, 24)]),
                           [5, 6, 7, 8, 9],
                           [3, 7, 11, 14],
                           circle, lt.new('graphic/down-for.pdf'), lt.new('result/graphit-down-for.tex')),
                          (lt.file('data/graphite-down-back'),
                           np.array([          (61, 84), (82, 83),
                                               (60, 71), (81, 69), (100, 66),
                                     (37, 58), (59, 56), (79, 53), ( 98, 49),
                                     (36, 42), (57, 40), (77, 37),
                                     (34, 25), (55, 23)]),
                           [5, 6, 7, 8],
                           [4, 7, 10, 12],
                           circle, lt.new('graphic/down-back.pdf'), lt.new('result/graphit-down-back.tex')),
                          lt.new('result/C_down.tex'), lt.new('result/C_down_error.tex'),
                          lt.new('result/F_down.tex'), lt.new('result/F_down_error.tex'))

v = np.array([(F_up[i] - F_down[i]) * s / t / (C_up[i][1] + C_down[i][1]) for i in range(2)])

g = np.array([F_up[i] - v[i] * C_up[i][1] * t / s for i in range(2)])

d_up = np.array([v[i] * C_up[i][1] * t / s for i in range(2)])
d_down = np.array([v[i] * C_down[i][1] * t / s for i in range(2)])

v_ges = lt.umean(v, axis=0)
v_ges_norm = unc.unumpy.sqrt(np.dot(v_ges, v_ges))

α = 1 / L * v_ges_norm * 3600

render_matrix(v.T, lt.new('result/v.tex'), lt.new('result/v_error.tex'))
render_matrix(g.T, lt.new('result/g.tex'), lt.new('result/g_error.tex'))
render_matrix(d_up.T, lt.new('result/d_up.tex'), lt.new('result/d_up_error.tex'))
render_matrix(d_down.T, lt.new('result/d_down.tex'), lt.new('result/d_down_error.tex'))
render_matrix(np.resize(v_ges, (2, 1)), lt.new('result/v_ges.tex'), lt.new('result/v_ges_error.tex'))
render_matrix(np.resize(v_ges_norm, (1, 1)), lt.new('result/v_ges_norm.tex'), lt.new('result/v_ges_norm_error.tex'))
render_matrix(np.resize(α * 1e6, (1, 1)), lt.new('result/α.tex'), lt.new('result/α_error.tex'))

g1 = unc.unumpy.sqrt(np.dot(g[0], g[0]))
g2 = unc.unumpy.sqrt(np.dot(g[1], g[1]))
g_angle = unc.unumpy.degrees(unc.unumpy.arccos(np.dot(g[0], g[1]) / g1 / g2))
render_matrix(np.resize(g1, (1, 1)), lt.new('result/g1.tex'), lt.new('result/g1_error.tex'))
render_matrix(np.resize(g2, (1, 1)), lt.new('result/g2.tex'), lt.new('result/g2_error.tex'))
render_matrix(np.resize(g_angle, (1, 1)), lt.new('result/g_angle.tex'), lt.new('result/g_angle_error.tex'))
