from labtools import *

def matrix_table(m):
    format = ["S[table-format=" + labtools.table.common.table_format(nums) + "]" for nums in m.T]
    s = r"\begin{array}{@{} " + " ".join(format) + " @{}}"
    s += r"\\".join([" & ".join(r) for r in m])
    s += r"\end{array}"
    return s

def return_matrix(m):
    vs = np.empty(np.shape(m), np.object)
    es = np.empty(np.shape(m), np.object)

    for i, n in np.ndenumerate(m):
        v = n.nominal_value
        e = n.std_dev

        es[i] = lt.round_figures(e, 1)
        vs[i] = lt.round_to(v, e, 1)

    vt, et = matrix_table(vs), matrix_table(es)
    return vt, et

def render_matrix(m, vf, ef):
    vt, et = return_matrix(m)
    open(vf, 'w').write(vt)
    open(ef, 'w').write(et)
