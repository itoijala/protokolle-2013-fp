from labtools import *
from matplotlib.patheffects import Stroke
from stm import matrix_table, render_matrix

def process(data, points, figname, vname):
    x, y, z = np.loadtxt(data, unpack=True)
    z = np.reshape(z, (128, 128))

    x = x[:128]
    y = y[::128]
    Δ = x[1] - x[0]

    b1_index = np.array([points[0] - points[1], points[3] - points[2]])
    b2_index = np.array([points[2] - points[1], points[3] - points[0]])

    b1 = b1_index * Δ
    b2 = b2_index * Δ
    B = np.array([b1, b2])

    open(vname, "w").write(" & ".join([", ".join([r"$\g({})$".format(matrix_table(np.resize([lt.round_places(v3, 0) for v3 in v2], (2, 1)))) for v2 in v1]) for v1 in B]))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    for point in points:
        ax.add_patch(plt.Circle(point, 15, fill=False, color='b'))
        ax.plot([point[0]], [point[1]], 'bx')

    arr = ax.arrow(points[1][0], points[1][1], points[0][0] - points[1][0], points[0][1] - points[1][1], length_includes_head=True, color='r', head_width=1.5)
    arr.set_path_effects([Stroke(joinstyle="miter", capstyle='butt')])
    arr = ax.arrow(points[2][0], points[2][1], points[3][0] - points[2][0], points[3][1] - points[2][1], length_includes_head=True, color='r', head_width=1.5)
    arr.set_path_effects([Stroke(joinstyle="miter", capstyle='butt')])
    arr = ax.arrow(points[1][0], points[1][1], points[2][0] - points[1][0], points[2][1] - points[1][1], length_includes_head=True, color='r', head_width=1.5)
    arr.set_path_effects([Stroke(joinstyle="miter", capstyle='butt')])
    arr = ax.arrow(points[0][0], points[0][1], points[3][0] - points[0][0], points[3][1] - points[0][1], length_includes_head=True, color='r', head_width=1.5)
    arr.set_path_effects([Stroke(joinstyle="miter", capstyle='butt')])

    im = ax.imshow(z, origin='lower', cmap=mpl.cm.afmhot, norm=mpl.colors.Normalize(-3, 3))
    ax.xaxis.set_major_formatter(lt.SiFormatter(0, factor=Δ))
    ax.yaxis.set_major_formatter(lt.SiFormatter(0, factor=Δ))
    ax.set_xlabel(r'$\silabel{x}{\nano\meter}$')
    ax.set_ylabel(r'$\silabel{y}{\nano\meter}$')
    cb = fig.colorbar(im, ax=ax, format=lt.SiFormatter(1))
    cb.set_label(r'\silabel{z}{\nano\meter}')
    fig.savefig(figname)

    return B

B_uf = process(lt.file('data/calibration-up-for'),
               np.array([( 75, 110),
                         ( 25,  78),
                         ( 70,  42),
                         (118,  77)]),
               lt.new('graphic/calibration-up-for.pdf'), lt.new('result/calibration-up-for.tex'))
B_ub = process(lt.file('data/calibration-up-back'),
               np.array([( 65, 110),
                         ( 18,  78),
                         ( 60,  42),
                         (113,  77)]),
               lt.new('graphic/calibration-up-back.pdf'), lt.new('result/calibration-up-back.tex'))
B_df = process(lt.file('data/calibration-down-for'),
               np.array([( 75, 105),
                         ( 23,  68),
                         ( 70,  32),
                         (115,  67)]),
               lt.new('graphic/calibration-down-for.pdf'), lt.new('result/calibration-down-for.tex'))
B_db = process(lt.file('data/calibration-down-back'),
               np.array([( 65, 105),
                         ( 18,  68),
                         ( 60,  32),
                         (113,  67)]),
               lt.new('graphic/calibration-down-back.pdf'), lt.new('result/calibration-down-back.tex'))

B = lt.mean(np.hstack((B_uf, B_ub, B_df, B_db)), axis=1).T

a1 = np.array([160,   0])
a2 = np.array([  0, 160])
A_inv = np.array([[1 / 160, 0], [0, 1 / 160]])

S = np.dot(B, A_inv)
S_inv = unc.unumpy.ulinalg.inv(S)

np.savetxt(lt.new('result/S_inv'), lt.vals(np.resize(S_inv, 4)))
np.savetxt(lt.new('result/S_inv_error'), unc.covariance_matrix(np.resize(S_inv, 4)))

render_matrix(B, lt.new('result/B.tex'), lt.new('result/B_error.tex'))
render_matrix(S_inv, lt.new('result/S_inv.tex'), lt.new('result/S_inv_error.tex'))
