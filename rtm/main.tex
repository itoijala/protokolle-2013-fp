\input{header/report.tex}

\addbibresource{lit.bib}

\SetExperimentNumber{RTM}

\begin{document}
  \maketitlepage{Rastertunnelmikroskopie}{26.03.2013}{30.05.2013}
  \section{Ziel}
    Mit einem Rastertunnelmikroskop wird eine HOPG-Probenoberfläche untersucht.
    Die Messdaten werden dazu verwendet, die HOPG-Gittervektoren zu bestimmen.
    Dies gibt Aufschluss über die Funktionsweise und den Betrieb eines Rastertunnelmikroskops (RTM).
  \section{Theorie}
    \subsection{Funktionsweise eines Rastertunnelmikroskops}
      Elektronen können durch klassisch unüberwindbare Potentiale tunneln, da ihre Wellenfunktion bis hinter eine endliche Potentialschwelle reicht.
      Für den Tunnelstrom zwischen zwei Elektroden gilt in erster Näherung
      \begin{eqns}
        I_\t{T} &\propto& \frac{U}{d}\exp(-K d \sqrt{\mean{φ}}) \eqlabel{eqn:tunnelstrom} \\
        K &=& \SI{1.025}{\per\angstrom\per\electronvolt\tothe{1/2}} ,
      \end{eqns}
      wobei $U$ die angelegte Spannung zwischen den Elektroden und $\mean{φ}$ die mittlere Austrittsarbeit des Materials sind.
      Wesentlich ist die exponentielle Abhängigkeit vom Abstand $d$ der Elektroden.

      Das RTM produziert das Abbild einer festen, leitenden Oberfläche mit atomarer Auflösung, indem es mit einer winzigen Metallspitze über die Oberfläche fährt und den Tunnelstrom der Elektronen im Abhängigkeit von der Position des Messkopfes auf der Probe misst.
      Die Spitze wird dabei von Piezokristallen bewegt, die über Spannungen gesteuert werden.
      Indem der Tunnelstrom konstant gehalten wird, kann die Spannung $U_z$, die die Spitze in $z$-Richtung bewegt, als Funktion der analog definierten Spannungen $U_x$ und $U_y$ gemessen werden.
      Der Tunnelstrom ändert sich normalerweise um eine Größenordnung für Änderungen des Abstandes in der Größenordnung von \SI{1}{\angstrom}.

      Laut Gleichung \eqref{eqn:tunnelstrom} ist der Tunnelstrom abhängig von den Strukturen der Oberfläche und Variationen der Austrittsarbeit.
      Bei der normalen Messung der Oberflächenform wird von einer konstanten Austrittsarbeit ausgegangen.
      Dadurch werden bei Konstanthalten des Tunnelstroms Änderungen der Austrittsarbeit durch korrespondierende Abstandsänderungen zwischen Probe und Sensor ausgeglichen.

      Das Elektron kann von der Metallspitze zur Halbleiteroberfläche oder zurück tunneln, abhängig von dem Vorzeichen der angelegten Spannung zwischen Spitze und Probe.
      Dadurch kann mehr Information über die elektronische Struktur gewonnen werden, indem das RTM Signal in Abhängigkeit von dem Vorzeichen und der Größe der Spitzen-Proben-Spannung untersucht wird.
      Für positive Spannungen können die Elektronen nur von besetzten Metallzuständen in leere Oberflächenzustände oder Leitungsbandzustände im Halbleiter tunneln.
      Im entgegengesetzten Fall ist elastisches Tunneln von dem Metall in den Halbleiter nicht möglich, da die Fermikante zu weit entfernt ist, sodass nur besetzte Zustände erreicht werden können.
      In diesem Fall stammt der gemessene Tunnelstrom von der besetzten Oberflächen- oder Valenzbandzuständen in dem Halbleiter.
      Durch diesen Freiheitsgrad können besetzte oder unbesetzt Zustände der Probe untersucht werden \cite{luth}.
      Dieser Sachverhalt ist in Abbildung \ref{fig:thorn} veranschaulicht.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/rtm.pdf}
        \caption{Tunnelstromrichtung in Abhängigkeit der Spannung zwischen Metallspitze und \mbox{Material} \cite{luth}}
        \label{fig:thorn}
      \end{figure}
    \subsection{Graphit und HOPG}
    \label{graphite}
      In den meisten Fällen besitzt Graphit in der Natur ein hexagonales Gitter.
      Dabei werden die Atome innerhalb der Wabenstruktur durch kovalente Bindungen zusammengehalten; zwischen den aufeinander liegenden Ebenen wirken Van-der-Waals-Kräfte.
      Die Ebenen sind um eine halbe Gitterkonstante zueinander verschoben, woraus eine periodische Schichtung der Ebenen A und B mit der Gestalt ABABA ensteht.
      Der nächste Nachbar in dieser Konfiguration ist $\SI{1.42}{\angstrom}$ entfernt.
      Die Gitterkonstante beträgt innerhalb der Ebenen $\SI{2.46}{\angstrom}$, der Abstand zweier sequentiell gestapelten Ebenen beträgt $\SI{3.35}{\angstrom}$, woraus eine Periodizität der Ebenen nach $\SI{6.7}{\angstrom}$ folgt.

      Da monokristalline Graphitkristalle in der Natur relativ klein und selten sind, wird bei RTM-Experimenten so genanntes \enquote{highly orientated pyrolytic graphite}(HOPG) verwendet.
      Dieses polykristalline Material mit einer hexagonalen Struktur hat eine relativ große Korngröße von \SIrange{3}{10}{\micro\meter} und die Achse, die die Flächensequenz senkrecht schneidet ($c$-Achse) hat eine gute Orientation, weicht also von der theoretischen Lage um weniger als $\SI{2}{\degree}$ ab.
      Es bietet gegenüber natürlichem Graphit den weiteren Vorteil, dass es einen verschwindenden Prozentsatz an rhomboedrischen Strukturen, also Schichtsequenzen der Form ABCABCA hat; dies erleichtert den experimentellen Zugang sehr.
      Des Weiteren kann die Probe leicht durch Abtragen von Graphitschichten mit einem Klebeband gereinigt werden und das HOPG ist robust gegen chemische Reaktionen \cite{wiesen}.
    \subsection{Interpretation der RTM-Bilder bei Graphit und Fehlerquellen bei der Messung}
      \label{errors}
      Im Allgemeinen sind RTM Bilder stark von der lokalen Elektronendichte der Zustände beeinflusst.
      In den ersten Messungen von Graphit mit einem RTM wurde ein Abstand von circa $\SI{2.5}{\angstrom}$ zwischen den Maxima des RTM-Bildes beobachtet, was darauf schließen ließ, dass nur jedes zweite Atom prominent war; es handelt sich hierbei um die Carbonflächenasymetrie.
      Neben der Carbonflächenasymetrie trat des Weiteren ein starker Abfall des maximal gemessenen Proben-Spitzen-Abstand bei konstantem Strom mit Erhöhung der zwischen Probe und Spitze angelegten Spannung auf.
      Gelegentlich treten in RTM Bildern sehr große Furchen auf, deren Größe basierend auf der Topologie nicht erklärbar ist.
      Ein Grund hierfür sind elastische Deformationen der Graphitoberfläche induziert von atomaren Kräften zwischen Tastspitze und Probenoberfläche.
      Das heißt, dass das RTM Bild nicht nur durch die lokale Zustandsdichte, sondern auch durch Spitzen-Oberflächen-Wechselwirkung, insbesondere bei einer kleinen Spitzen-Oberflächen-Trennung, beeinflusst wird.
      Eine Konsequenz dieser Wechselwirkung ist, dass die Kräfte zwischen der Spitze und der Probe mmer dann berücksichtigt werden müssen, wenn weiche Proben wie Graphit oder andere geschichtete Materialien mit dem RTM bei kleineren Tunnellückenabständen untersucht werden.
      Des Weiteren ist zu beachten, dass Bilder auch dann erzeugbar sind, wenn die Spitze an einem Punkt im Kontakt mit der Probe steht.
      Die Potentialbarriere ist also nicht mehr da; der Tunnelstrom ist in diesem Falle periodisch.
      Eine weitere Schwierigkeit in der Interpretation ist das Auftreten von dreieckigen Reihen von Dreiecken, Honigwabenreihen und dreieckige Reihen von Ellipsen oder auch geraden, reihenartigen Strukturen.
      Diese sind auf die Tatsache zurückzuführen, dass die Tastspitze im Experiment aus mehreren Spitzen besteht, was zu einer Veränderung der relativen Amplituden und eine Verschiebung der relativen Phasen der Komponenten führt; diese können die Gestalt und die Amplitude der Maxima verändern.
      Eine Erklärung für die auftretenden Reihen können spezielle elektronische Levels in der Spitze sein, die beim Tunneln aktiv sind.
      Die Honigwabenstruktur, die jedes Atom des Graphits gleichermaßen abbildet, ist bis heute noch nicht erklärt \cite{wiesen}.

      Auf einem gutem TopView-Bild befindet sich ein Muster aus weißen, grauen und schwarzen Stellen.
      Diese stehen für hohe und mittlere Werte, sowie Vertiefungen.
      Im Graphit gibt es zwei verschiedene Gitterplätze, solche mit einem Nachbarn in der darunterliegenden Lage (grau) und solche ohne (weiß).
      Dies hat zu Folge, dass die Leitfähigkeit der Atome an der Oberfläche je nach Gitterplatz leicht varriert und diejenigen Atome ohne Nachbarn leicht erhöht erscheinen.
      Dies wiederum resultiert in einer Gitterkonstante für Graphit mit einem zu großem Wert $\SI{0.25}{\nano\meter}$ bei Vermessung der Distanz zwischen den hohen Kugeln \cite{stm}.
    \subsection{Die Scaneinheit}
      Das Abfahren der Probe erfolgt in einem Rasterverfahren.
      Das besondere ist, dass hierbei die Probe auf einer Zeile des Rasters immer nur in einer Richtung durchfahren wird; diese Richtung wir als Fast-Scan-Direction bezeichnet, das Wechseln der Zeilen als Slow-Scan-Direction.
      Dieses Verhalten dient dazu, Scanfehler, hervorgerufen durch Hysterese, zu vermeiden.
      Bei der Fahrt des Scankopfes werden die Daten in regelmäßigen Abständen genommen und digitalisiert.
      Wird wie in diesem Fall die Höhe konstant gehalten, so sind die relevanten Daten der Tunnelstrom.
      Der Abstand zwischen den Datenpunkten wird als Schrittweite bezeichnet.
      Für gewöhnlich ist die Anzahl der Linien gleich der Anzahl der Punkte in einer Zeile des Rastergitters, was in einer quadratisch abgescanten Fläche resultiert \cite{howland}.
    \subsection{Die piezoelektrischen Bauteile des Scanners}
      Piezoelektrische Materialen sind Keramiken, die auf angelegte Spannungen reagieren, in dem sie ihre Dimensionen ändern.
      Umgekehrt erzeugen sie auch eine Spannung bei Deformation.
      Auf diese Art und Weise kann ein Scanelement in alle drei Raumrichtungen gesteuert werden.

      Die Bauteile werden in der Regel aus Bleizirkoniumtitanaten (PZT, je nach gewünschter Eigenschaften mit verschiedenen Dotierstoffen versetzt) gefertigt.
      Nachdem sie aus diesen Pulvern gesintert wurden, entsteht ein polykristalliner Festkörper.
      Jeder dieser Kristalle hat ein eigenes Dipolmoment und ist zufällig in dem Kristall angeordnet.
      Durch einen Prozess namens Poling werden die Kristalle angeordnet, was den Scanner befähigt sich zu bewegen.
      Hierbei wird bei $\SI{200}{\celsius}$ ein Gleichstrom hindurchgeschickt, der die Dipole ausrichtet; beim Erkalten werden sie fixiert.
      Indem der Scanner gelegentlich benutzt wird, wird die Polarisation im Scanner aufrecht gehalten, des Weiteren darf er Temperaturen größer als $\SI{200}{\celsius}$ nicht überschreiten.
      Die maximale Scanfläche die von einem Piezoelement erreicht werden kann hängt von der Länge, dem Durchmesser, der Wanddicke und den Dehnungskoeffizienten des Scanelementes ab \cite{howland}.
    \subsection{Nichtlinearitäten des Scanners}
      \label{non-linearities}
      In erster Näherung ändert der Piezokristall linear seine Länge mit der angelegten Spannung.
      Dabei ist die Längenänderung
      \begin{eqn}
        s = δ E, \eqlabel{eqn:linear_extension}
      \end{eqn}
      wobei $δ$ der materialspezifische Dehnungskoeffizient ist \cite{howland}.
      \subsubsection{Intrinsische Nichtlinearität}
        Wird in der Realität die Ausdehnung gegen die Spannung aufgetragen, so verläuft der Graph s-förmig um die ideale Kurve \eqref{eqn:linear_extension}.
        Der Quotient aus maximaler Abweichung $\Del{y}$ von der Idealgeraden und der idealen Ausdehnung $y$ wird als intrinsische Nichtlinearität bezeichnet, diese reicht typischerweise von \SIrange{2}{25}{\percent} in piezoelektrischen Materialien, die in RTM-Systemen verwendet werden.
        Diese Nichtlinearität induziert eine Verzerrung des Scanrasters, was in einer Verzerrung der untersuchten Gitterstruktur resultiert.
        Des Weiteren kann sie bei der Messung der Höhe in $z$-Richtung Fehler verursachen.
        Vor der Messung wird daher auf eine bestimmte Höhe kalibriert, das heißt die Spannung bei mehreren bekannten Höhenstufen abgelesen, woraus für diese spezielle Probenhöhe die Dehnungskoeffizienten $δ$ berechnet werden können \cite{howland}.
      \subsubsection{Hysterese}
        Piezoelektrische Keramiken zeigen außerdem ein Hystereseverhalten.
        Die Hysterese eines piezoelektrischen Scanners ist das Verhältnis aus maximaler Differenz der Ausdehnungen bei Erhöhen oder Senken der Spannung und der idealen linearen Ausdehnung \eqref{eqn:linear_extension}, sie beträgt ungefähr \SI{20}{\percent}.
        Bei Vergleich von Daten, die bei entgegengesetzten Fast-Scan-Directions ermittelt wurden, fällt eine Verschiebung im Datensatz auf, aus der die Hysterese abgeleitet werden kann.
        Bei Auftreten dieser Hysterese in $z$-Richtung kommt es zu fehlerhaften Oberflächenprofilen \cite{howland}.
      \subsubsection{Creep}
        Wird die Spannung plötzlich geändert, so ändert das piezoelektrische Material seine Dimensionen nicht sofort vollständig, sondern in zwei Schritten.
        Die erste Änderung tritt in weniger als einer Millisekunde, die zweite nach längerer Zeit auf.
        Der Creep wird als Verhältnis der initialen und der sekundären Längenänderung definiert und wird normalerweise mit der dazwischen liegenden Zeitdauer $T_{\t{cr}}$ behaftet.
        Typische Zahlenwerte erstrecken sich für den Creep von \SIrange{1}{20}{\percent} und für $T_\t{cr}$ von \SIrange{10}{100}{\second}.
        Vor der Messung muss auch die Scanzeit kalibriert werden, da unterschiedliche Scangeschwindigkeiten verschiedene Längenskalen beim Einsetzen des Creeps erzeugen.
        Ein weiterer Effekt des Creeps kann eine auftretende Verzögerung bei der Datenaufnahme sein.
        Wird beispielsweise für einen Zoom der Scanner neu ausgerichtet, kann dies nicht plötzlich geschehen sondern dauert mindestens $T_\t{cr}$.
        In $z$-Richtung muss wegen diesem Effekt eine Gegenspannung sofort nach der quasi-instantanen Änderung angelegt werden, die der nachträglichen Längenänderung vorbeugt.
        Beim Abbilden der Flächenmorphologie äußert sich der Effekt, indem bei einer Struktur auf der einen Seite Erhöhungen und auf der anderen Seite Vertiefungen auftreten.
        Dieser Effekt wird durch Umkehren der Scanrichtung identifiziert und kann dann berücksichtigt werden \cite{howland}.
      \subsubsection{Alterung}
        Der Dehnungskoeffizient $δ$ von piezoelektrischen Materialen ändert sich exponentiell mit der Zeit und der Benutzung.
        Die Alterungsrate ist die Änderung von $δ$ pro Zeitdekade.
        Wird der Scanner über längere Zeit nicht benutzt, so nimmt die Längenänderung bei fester Spannung über die Zeit ab.
        Wird der Scanner jedoch regelmäßig benutzt, so nimmt die Längenänderung langsam, aber stetig zu.
        Dieser Effekt steht in direkter Beziehung zu der Anzahl der Dipole, die ausgerichtet sind.
        Mit der Zeit richten sie sich zufällig aus, was durch erneutes Fließen eines Stromes rückgängig gemacht wird.
        Diese Tatsache erschwert Vergleiche von Abmessungen morphologischer Strukturen bei Vergleich von Messungen, zwischen denen große Zeitintervalle liegen \cite{howland}.
      \subsubsection{Kreuzkopplung}
        Kreuzkopplung bezieht sich auf die Tatsache, dass es in der Regel nicht möglich ist, diverse Achsen separat anzusprechen.
        Dieser komplexer Sachverhalt hat viele Gründe, wie beispielsweise Inhomogenitäten angelegter Felder und das Vorhandensein komplizierter Dehnungstensoren statt Dehnungskonstanten.
        Insbesondere ist jedoch zu beachten, dass bei konstanter $z$-Ausdehnung der Scanner in einer Parabel scannt, anstatt auf einer geraden Fläche.
        Die $z$-Achse muss daher angeglichen werden.
        Dieser Effekt äußert sich im erzeugten Abbild der Struktur, indem flache Strukturen als schüsselförmig erscheinen.
        Mit Hilfe von Software können die gekrümmten Strukturen begradet werden, auch hier empfiehlt sich eine Eichung mittels einer bekannten Krümmung.
        Jedoch tritt auch hier das Problem auf, dass auf die Krümmung der bekannten Struktur, die ohnehin schon auftretende Krümmung addiert wird \cite{howland}.
    \subsection{Methoden zur Eliminierung von Nichtlinearitäten}
      \subsubsection{Softwareverbesserung}
        Bei einer Softwareverbesserung vergleicht das System die gemessenen Daten mit den bekannten Eigenschaften des Gitters.
        Anhand dieser gespeicherten Daten können Nichtlinearitäten ausgeglichen werden.
        Eine weitere Möglichkeit besteht darin, bekannte, störende Effekte durch theoretische Rechnung abzuziehen.
        Softwarelösungen sind schnell und kostengünstig konstruiert, beheben die Nichtlinearitäten aber nur teilweise.
        Sie helfen nur bei Problemen, die vorher kalibriert wurden.
        Die gesamte Nichtlinearität wird hier auf \SI{10}{\percent} reduziert \cite{howland}.
      \subsubsection{Hardwareverbesserungen}
        Bei Hardwareverbesserungen wird die aktuelle Position des Scanners durch Sensoren ermittelt.
        Hierbei wird die in Echtzeit gemessene mit der gewünschten Position verglichen.
        Gegebenenfalls wird eine automatische Nachjustierung vorgenommen.
        Die gesamte Nichtlinearität wird hier auf bis unter \SI{1}{\percent} reduziert.
        Dies geschieht im Einzelnen durch optische, kapazitive oder Dehnungseichung \cite{howland}.
  \section{Aufbau}
    Eine feine Platinspitze ist in einer Halterung befestigt, die durch Piezokristalle in alle drei Raumrichtungen bewegt werden kann.
    Die Probe wird ebenfalls auf ein Piezoelement geschoben, sodass sie bis auf einen Abstand von $\SI{1}{\nano\meter}$ an die Messspitze herangefahren werden kann.
    Im Mikroskop kann ein Regelkreis den $z$-Piezokristall so steuern, dass der Tunnelstrom und somit der Abstand zwischen Probe und Spitze automatisch konstant bleibt (constant current mode); die Bewegungen der Messspitze werden in Bilder umgesetzt.
    Mit dem easyScan RTM können Experimente an Luft ausgeführt werden, außerdem lassen sich alle Funktionen vom Computer aus steuern \cite{stm}.
  \section{Durchführung}
    \subsection{Herstellen und Einbauen einer Messspitze}
      Für die Herstellung einer Spitze aus Platindraht werden ein Seitenschneider, eine Flachzange und eine Pinzette benötigt.
      Vor der Herstellung der Spitze müssen die Werkzeuge mit etwas Ethanol gereinigt werden, auch der Platindraht darf nur mit entfetteten Werkzeugen angefasst werden.
      Anschließend wird unter Fixierung mit der Flachzange ein $\SI{5}{\milli\meter}$ langes Stück abgeschnitten.
      Das Stück wird mit der Flachzange weiterhin festgehalten, am anderen Ende ein Seitenschneider möglichst schräg am Draht angesetzt und Fasern des Drahtes abgezogen.
      Die geschnittene Spitze darf mit nichts in Berührung kommen, ebenfalls dürfen die vergoldeten Federn im offenen Teil des Meskopfes nicht hochgebogen werden.
      Abschließend wird der Draht mit einer Pinzette unter eine fixierende Feder geschoben, sodass der Draht \SIrange{2}{3}{\milli\meter} heraus ragt \cite{stm}.
    \subsection{Vorbereiten und Einsetzen der Probe}
      Nur elektrisch leitfähiges Material kann untersucht werden.
      Das hier benutzte Graphit wird durch Abziehen einer Schicht mit einem Klebestreifen gereinigt.
      Größere Graphitflocken müssen vor der Messung mit einer Pinzette entfernt werden.
      Der Probenhalter sollte nur am Kuststoffende mit bloßen Fingern berührt werden.
      Die Probe wird mit der Pinzette auf die magnetische Fläche des Probenhalters gelegt.
      Anschließend wird der Probenhalter in einer Führung in die Öffnung des Messkopfes eingefahren, sodass er die Messspitze nicht berührt.
      Um den Piezomotor nicht zu schädigen wird der Probenhalter erst auf die Schienen und anschließend auf die Auflagen des Piezomotors aufgesetzt \cite{stm}.
    \subsection{Annäherung der Probe}
      Zunächst wird der Probenhalter vorsichtig bis auf einen Millimeter Abstand zur Messspitze geschoben.
      Dabei sollte die Spitze auf eine spiegelglatte Stelle der Probenfläche gerichtet sein.
      Eventuell wird der Probenhalter in eine geeignete Position dafür gedreht und die Abdeckhaube vorsichtig ohne Anstoßen über den Messkopf gezogen.
      Die restliche Annäherung erfolgt elektronisch mit den Piezomotoren \cite{stm}.
    \subsection{Qualität der Bilder}
      Wenn Temperaturschwankungen die Messung beeinflussen, werden die Bilder verzogen, oder die verschiedenen Messrichtungen sind durch gestauchte oder gestreckte Gitterstrukturen zu erkennen.
      Kleinste Temperaturschwankungen haben schon große Wirkung bei der Ausdehnung des Probenhalters.
      Des Weiteren kann während der Messung ein Partikel aufgelesen werden, oder die Bilder im TopView aus verschiedenen Streifen bestehen oder verschmiert sein.
      Schließlich sollte nicht jedes Bild anders aussehen, oder die Messlinien im LineView unruhig und die Bilder im TopView unscharf sein \cite{stm}.
    \subsection{Messung}
      Die Messung besteht aus zwei Teilen; diese sind die Vermessung des Kalibrationsgitters und des HOPG-Gitters.
      Die Messung am Kalibrationsgitter dient zur Skalierung der HOPG-Messung.

      Bei beiden Messungen besteht die Möglichkeit das Koordinatensystem elektronisch anzupassen, sodass die Probe scheinbar senkrecht unter der Messspitze liegt.
      Erscheint nach Start der Messung im LineView des easyScan Programms die Messlinie unruhig, so ist der Messkontakt schlecht und die Spitze ist zu stumpf oder instabil und musst ausgetauscht werden.\cite{stm}.

      Alle Proben, das heißt das HOPG und das Kalibrationsgitter werden in den Moden Up-Forward (UF), Up-Backward (UB), Down-Forward (DF) und Down-Backward(DB) gemessen \cite{anleitungRTM}.

      Um den thermischen Drift zu minimieren, wird so lange gescannt, bis sich die Up-Bilder nicht mehr von den Down-Bildern unterscheiden.
      Dabei müssen die Daten zwischendurch abgespeichert werden, um qualitativ gute Messwerte nicht zu verlieren; es besteht die Gefahr, dass eine plötzlich eintretende Spitzenzustandsänderung die Qualität der Aufnahmen plötzlich verändert.
      Bei Betrachtung der Aufnahmen soll des Weiteren versucht werden Gittervektoren in der hexagonalen Gitterstruktur zu erkennen.
      Für die Minimierung des Messfehlers werden ausreichend viele Atome sichtbar gemacht, sodass ein Gittervektor über mehrere Einheitszellen gelegt und dann über die Anzahl der Zellen gemittelt werden kann.
      Während der Messung muss des Weiteren beachtet werden, dass die Bilder nahezu driftfrei sind; dazu wird ein Leerlaufscan von ca. $\SI{1}{\hour}$ durchgeführt.
      Am Ende werden alle Messdaten auf einen USB-Stick kopiert \cite{anleitungRTM}.
      \subsubsection{Messung der HOPG-Probe}
        \label{messung_hopg}
        Vor Beginn dieser Messung werden die Bildgröße (Scanrange) und die digitale Auflösung entlang der $z$-Achse ($z$-Range) auf einen Wert von $\SI{200}{\nano\meter}$ eingestellt.
        Die Scangeschwindigkeit beträgt \SI{0.07}{\second} pro Linie.
        Zu beachten ist, dass der Abstand zwischen Probe und Spitze fest ist und während der Messung nicht verändert wird.
        Nachdem die Tunnelspitze angenähert und die Probe ausgerichtet ist, können Scanrange und $z$-Range verringert werden.
        Dies geschieht schrittweise auf einen Wert von \SIrange{1}{2}{\nano\meter} für die Scanrange, gleichzeitig dazu auf einen Wert von ungefähr $\SI{0.7}{\nano\meter}$ für die $z$-Range \cite{anleitungRTM}.
      \subsubsection{Messung des Kalibrationsgitters}
        Bei dieser Messung ist zu beachten, dass der strukturierte Bereich auf der Probe auf eine Fläche von $\SI{4}{\milli\meter\squared}$ begrenzt ist; er ist mit dem Auge beim Schwenken im Licht auszumachen.
        Die Tunnelspitze wird so eingestellt, dass sie nur den strukturierten Bereich abscannt.
        Die Messung erfolgt hier im Wesentlichen analog zu \ref{messung_hopg}.
        Allerdings werden hier wegen der sich unterscheidenden Strukturgröße die Scanrange auf ihr Maximum, das heißt auf $\SI{1700}{\nano\meter}$ gestellt.
        Da Schwankungen in der Qualität des Gitters im strukturierten Bereich auftreten können, muss die Spitze an so eine Stelle gebracht werden, sodass die Bildgröße auf \SIrange{200}{500}{\nano\meter} reduziert werden kann, da der Hysterese-Effekt mit der Auslenkung der Piezos und somit direkt mit der Bildgröße verknüpft wird.
        Die $z$-Range wird während der Messung von $\SI{200}{\nano\meter}$ soweit reduziert, bis eine genügend hohe Auflösung erreicht ist, das heißt er liegt im Bereich von \SIrange{10}{25}{\nano\meter}.
        Die Scangeschwindigkeit wird hier auf einen größeren Wert von $\SI{0.3}{\second}$ pro Line eingestellt \cite{anleitungRTM}.
  \section{Auswertung}
    \subsection{Extrahierung der Gittervektoren}
      Das RTM misst die Höhe an den Punkten eines quadratischen Gitters der Größe $128 {\times} 128$.
      Die Umrechnung zwischen dem ganzzahligen Gitterplatz und der Koordinaten erfolgt über den Faktor $Δ$.

      Um Gittervektoren $\v{g}_1$ und $\v{g}_2$ aus einem Messbild zu extrahieren, werden die Mittelpunkte der Einheitszellen im Bild manuell markiert.
      Es werden zwei Richtungen gewählt, wodurch die Richtungen der Gittervektoren definiert sind.
    \subsection{Kalibrierung}
      \label{calibration}
      Um die Längenergebnisse des Mikroskops zu kalibrieren, wird ein Kalibrationsgitter mit den bekannten Gittervektoren
      \begin{eqn}
        \m{A} = \g(\v{a}_1 \; \v{a}_2) =
        \g({
          \begin{array}{@{} S[table-format=3.0] S[table-format=3.0] @{}}
            160 &   0 \\
              0 & 160
          \end{array}
        }) \, \si{\nano\meter}
      \end{eqn}
      verwendet.
      Der Zusammenhang zu den gemessenen Gittervektoren $\m{B}$ ist gegeben durch
      \begin{eqn}
        \m{B} = \m{S} \m{A}
      \end{eqn}
      mit der Skalierungsmatrix $\m{S}$.
      Für die Umrechnung der HOPG-Gittervektoren wird die Inverse der Skalierungsmatrix
      \begin{eqn}
        \m{S}^{-1} = \g(\m{B} \m{A}^{-1})^{-1}
      \end{eqn}
      benötigt.

      Da die Strukturen des Kalibrationsgitters groß sind, kann der thermische Drift vernachlässigt werden.
      Die Messbilder mit markierten Einheitszellen und eingezeichneten Vektoren befinden sich in den Abbildungen \ref{fig:calibration-up-for} bis \ref{fig:calibration-down-back}, wobei
      \begin{eqn}
        Δ = \frac{\SI{441}{\nano\meter}}{128}
      \end{eqn}
      ist.
      Die extrahierten Vektoren stehen in Tabelle \ref{tab:calibration}.

      Durch Mittelung über diese ergibt sich
      \begin{eqn}
        \m{B} = \g(\v{b}_1 \; \v{b}_2) = \g[\g(\input{result/B.tex}) \pm \g(\input{result/B_error.tex})] \, \si{\nano\meter} \\
      \end{eqn}
      und
      \begin{eqn}
        \m{S}^{-1} = \g(\m{B} \m{A}^{-1})^{-1} = \g(\input{result/S_inv.tex}) \pm \g(\input{result/S_inv_error.tex}) .
      \end{eqn}
      \begin{table}
        \caption{Vektoren aus der Kalibrationsmessung}
        \label{tab:calibration}
        \begin{tabular}{l c c}
          \toprule
             & $\v{b}_1$ & $\v{b}_2$ \\
          \midrule
          UF & \input{result/calibration-up-for.tex}    \\[0.5cm]
          UB & \input{result/calibration-up-back.tex}   \\[0.5cm]
          DF & \input{result/calibration-down-for.tex}  \\[0.5cm]
          DB & \input{result/calibration-down-back.tex} \\
          \bottomrule
        \end{tabular}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/calibration-up-for.pdf}
        \caption{UF-Rastertunnelmikroskopbild des Kalibrationsgitters und angenommene Mittelpunkte}
        \label{fig:calibration-up-for}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/calibration-up-back.pdf}
        \caption{UB-Rastertunnelmikroskopbild des Kalibrationsgitters und angenommene Mittelpunkte}
        \label{fig:calibration-up-back}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/calibration-down-for.pdf}
        \caption{DF-Rastertunnelmikroskopbild des Kalibrationsgitters und angenommene Mittelpunkte}
        \label{fig:calibration-down-for}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/calibration-down-back.pdf}
        \caption{DB-Rastertunnelmikroskopbild des Kalibrationsgitters und angenommene Mittelpunkte}
        \label{fig:calibration-down-back}
      \end{figure}
    \subsection{Graphit}
      Die Messbilder mit eingezeichneten Einheitszellenmittelpunkten und Vektoren befinden sich in den Abbildungen \ref{fig:up-for} bis \ref{fig:down-back}.
      Hier gilt
      \begin{eqn}
        Δ = \frac{\SI{1.58}{\nano\meter}}{128} .
      \end{eqn}
      Zur Berechnung eines Gittervektors aus einer Reihe von $N$ Punkten $\v{w}_i$ wird die Funktion
      \begin{eqn}
        χ^2 = \sum_{i = 0}^N \g\{
          \v{w}_i - \g[
          \begin{pmatrix}
            x \\
            y
          \end{pmatrix}
          + \frac{i}{N - 1}
          \begin{pmatrix}
            u \\
            v
          \end{pmatrix}
          ]\}^{\! 2}
      \end{eqn}
      bezüglich der Parameter $x$, $y$, $u$ und $v$ minimiert.
      Dann gilt für den Gittervektor
      \begin{eqn}
        \v{c}_i = \frac{1}{N - 1}
          \begin{pmatrix}
            u \\
            v
          \end{pmatrix} .
      \end{eqn}
      Die verwendeten Punkte und die Ergebnisse für die Gittervektoren stehen in Tabelle \ref{tab:graphit}.

      Bei der Mittelung wird die Up- von der Down-Messung unterschieden.
      Für die Vektoren $\v{c}$ und die kalibrierten Vektoren $\v{f}$ ergibt sich
      \begin{eqn}
        \m{C}_{\t{up}} = \g(\v{c}_{\t{up}, 1} \; \v{c}_{\t{up}, 2}) = \g[\g(\input{result/C_up.tex}) \pm \g(\input{result/C_up_error.tex})] \, \si{\nano\meter} \\
        \m{F}_{\t{up}} = \g(\v{f}_{\t{up}, 1} \; \v{f}_{\t{up}, 2}) = \m{S}^{-1} \m{C}_{\t{up}}
          = \g[\g(\input{result/F_up.tex}) \pm \g(\input{result/F_up_error.tex})] \, \si{\nano\meter}
      \end{eqn}
      und
      \begin{eqn}
        \m{C}_{\t{down}} = \g(\v{c}_{\t{down}, 1} \; \v{c}_{\t{down}, 2}) = \g[\g(\input{result/C_down.tex}) \pm \g(\input{result/C_down_error.tex})] \, \si{\nano\meter} \\
        \m{F}_{\t{down}} = \g(\v{f}_{\t{down}, 1} \; \v{f}_{\t{down}, 2}) = \m{S}^{-1} \m{C}_{\t{down}} \\
        = \g[\g(\input{result/F_down.tex}) \pm \g(\input{result/F_down_error.tex})] \, \si{\nano\meter} . \eqlabel{eqn:graphit_skalierung}
      \end{eqn}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/up-for.pdf}
        \caption{Rastertunnelmikroskopbild des UF-Scans und angenommene Gitterstruktur}
        \label{fig:up-for}
      \end{figure}
      \begin{landscape}
        \parbox[c][\textwidth][s]{\linewidth}{%
          \vfill
          \captionof{table}{Vektoren aus der Graphitmessung}
          \label{tab:graphit}
          \mbox{%
            \begin{tabular}{l c c c c}
              \toprule
                 & \multicolumn{2}{c}{$\v{c}_1$} & \multicolumn{2}{c}{$\v{c}_2$} \\
                 & $\v{w}_i / Δ$ & $\v{c}_1$ & $\v{w}_i / Δ$ & $\v{c}_2$  \\
              \midrule
              UF & \input{result/graphit-up-for.tex}    \\[0.5cm]
              UB & \input{result/graphit-up-back.tex}   \\[0.5cm]
              DF & \input{result/graphit-down-for.tex}  \\[0.5cm]
              DB & \input{result/graphit-down-back.tex} \\
              \bottomrule
            \end{tabular}
          }
          \vfill
        }
      \end{landscape}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/up-back.pdf}
        \caption{Rastertunnelmikroskopbild des UB-Scans und angenommene Gitterstruktur}
        \label{fig:up-back}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/down-for.pdf}
        \caption{Rastertunnelmikroskopbild des DF-Scans und angenommene Gitterstruktur}
        \label{fig:down-for}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/down-back.pdf}
        \caption{Rastertunnelmikroskopbild des DB-Scans und angenommene Gitterstruktur}
        \label{fig:down-back}
      \end{figure}
    \subsection{Driftkorrektur}
      Wenn die Temperatur an einer Stelle des Systems geändert wird, indem die Spitze oder die Probe gewechselt wird, so reagiert das System an dieser Stelle in Abhängigkeit von seinem Ausdehnungskoeffizienten mit einer Längen- oder Volumenänderung; dies wird als thermisches Driften bezeichnet.
      Der gemessene Gittervektor $\v{f}$ unterscheidet sich daher um einen Driftvektor $\v{d}$ vom richtigen Gittervektor $\v{g}$.
      Das Vorzeichen des Driftvektors ist abhängig von der Scanrichtung und kann zu einer Dehnung oder Kontraktion von $\v{g}$ führen.
      \begin{eqn}
        \v{f}_{\g\{\t{up}, \t{down}\}, i} = \v{g}_i ± \v{d}_{\g\{\t{up}, \t{down}\}, i} . \eqlabel{eqn:drift_up}
      \end{eqn}
      Für die Driftstrecke gilt
      \begin{eqn}
        \v{d} = \v{v} \Del{t} , \eqlabel{eqn:driftvektor}
      \end{eqn}
      wobei $\v{v}$ die Driftgeschwindigkeit und $\Del{t}$ die Zeit ist, die während der Aufnahme vergeht.
      Diese unterscheidet sich bei den verschiedenen Scanrichtungen.
      Bei der Fast-Scan-Direction entlang der $x$-Achse ist sie sehr kurz, der Drift kann daher vernachlässigt werden.
      Bei den Scans entlang der $y$-Achse, also Up und Down, ist sie um einen Faktor 100 größer und der Drift ist daher nicht mehr zu vernachlässigen.
      Die Zeit, die für die Aufnahme eines ganzen Bildes benötigt wird, ist das Produkt aus Anzahl der gescannten Linien in $y$-Richtung und der Scandauer einer Linie in $x$-Richtung.
      Bei konstanter Scangeschwindigkeit gilt
      \begin{eqn}
        \Del{t} = c_y \frac{t}{s} . \eqlabel{eqn:gittervektorzeit}
      \end{eqn}
      Dabei ist $s = 128 Δ$ die unskalierte Größe des Bildes und $t = 128 v_{\t{scan}}$ die Zeit, die ein Bildscan benötigt, wobei $v_{\t{scan}}$ die Scangeschwindigkeit ist.

      Aus \eqref{eqn:driftvektor} und \eqref{eqn:gittervektorzeit} folgt für den Driftvektor
      \begin{eqn}
        \v{d}_{\g\{\t{up}, \t{down}\}, i} = \v{v}_i c_{\g\{\t{up}, \t{down}\}, i, y} \frac{t}{s} . \eqlabel{eqn:letzter_driftvektor}
      \end{eqn}
      Aus \eqref{eqn:drift_up} und \eqref{eqn:letzter_driftvektor} ergibt sich ein lineares Gleichungssystem mit 4 Unbekannten und 4 Gleichungen.
      Daraus ergibt sich für die Driftgeschwindigkeit
      \begin{eqns}
        \v{v}_i &=& \g(\v{f}_{\t{up}, i} - \v{f}_{\t{down}, i}) \frac{1}{c_{\t{up}, i, y} + c_{\t{down}, i, y}} \frac{s}{t} \\
        \g(\v{v}_1 \; \v{v}_2) &=& \g[\g(\input{result/v.tex}) \pm \g(\input{result/v_error.tex})] \, \si{\nano\meter\per\second} .
      \end{eqns}
      Für die Driftvektoren ergibt sich
      \begin{eqns}
        \v{d}_{\g\{\t{up}, \t{down}\}, i} &=& \v{v}_i c_{\g\{\t{up}, \t{down}\}, i, y} \frac{t}{s} \\
        \g(\v{d}_{\t{up}, 1} \; \v{d}_{\t{up}, 2}) &=& \g[\g(\input{result/d_up.tex}) \pm \g(\input{result/d_up_error.tex})] \, \si{\nano\meter} \\
        \g(\v{d}_{\t{down}, 1} \; \v{d}_{\t{down}, 2}) &=& \g[\g(\input{result/d_down.tex}) \pm \g(\input{result/d_down_error.tex})] \, \si{\nano\meter} .
      \end{eqns}
      Zuletzt folgt für die Gittervektoren
      \begin{eqns}
        \v{g}_i &=& \v{f}_{\t{down}, i} + \v{v}_i c_{\t{down}, i, y} \frac{t}{s} \\
        \g(\v{g}_1 \; \v{g}_2) &=& \g[\g(\input{result/g.tex}) \pm \g(\input{result/g_error.tex})] \, \si{\nano\meter} \\
        g_1 &=& \g(\input{result/g1.tex} ± \input{result/g1_error.tex}) \, \si{\nano\meter} \\
        g_2 &=& \g(\input{result/g2.tex} ± \input{result/g2_error.tex}) \, \si{\nano\meter} \\
        \f{\sphericalangle}(\v{g}_1, \v{g}_2) &=& \g(\input{result/g_angle.tex} ± \input{result/g_angle_error.tex}) \si{\degree} .
      \end{eqns}

      Schließlich soll das Driftverhalten des gesamten Systems untersucht werden.
      Dafür wird die benötigte Driftgeschwindigkeit
      \begin{eqns}
        \v{v}_{\t{ges}} &=& \g[\g(\input{result/v_ges.tex}) \pm \g(\input{result/v_ges_error.tex})] \, \si{\nano\meter\per\second} \\
        v_{\t{ges}} &=& \g(\input{result/v_ges_norm.tex} \pm \input{result/v_ges_norm_error.tex}) \, \si{\nano\meter\per\second}
      \end{eqns}
      durch Mittelung aus $\v{v}_1$ und $\v{v}_2$ gewonnen.
      Als Vergleichsparameter wird der stoffspezifische Ausdehnungskoeffizient $α$ berechnet, für ihn gilt bei Raumtemperatur
      \begin{eqns}
        α &=& \frac{1}{L} \frac{\Del{L}}{\Del{T}} = \frac{1}{L} \frac{\Del{L}}{\Del{t}} \frac{\Del{t}}{\Del{T}} = \frac{1}{L} v_{\t{ges}} \frac{1}{\frac{\Del{T}}{\Del{t}}} \\
        &=& (\input{result/α.tex} \pm \input{result/α_error.tex}) \cdot \SI{e-6}{\per\kelvin} ,
      \end{eqns}
      wobei $L = \SI{5}{\milli\meter}$ die Länge des Festkörpers und $\Del{T} / \Del{t} = \SI{1}{\kelvin\per\hour}$ sind \cite{anleitungRTM}.
  \section{Diskussion}
    Der Wärmeausdehnungskoeffizient liegt deutlich über typischen Koeffizienten für Keramiken.
    Ein für Keramiken überdurchschnittlich großes $α$ findet sich bei dichtem Magnesiumoxid mit einem Wert von $\SI{13.5e-6}{\per\kelvin}$ \cite{keramik}.
    Dieser ist als mittlerer Ausdehnungskoeffizient bei einem Temperaturintervall von \SIrange{30}{10000}{\celsius} zu verstehen.
    Der hier ermittelte Ausdehnungskoeffizient übersteigt selbst den von Cadmium ($\SI{31.0e-6}{\per\kelvin}$ \cite{cadmium}), das unter den Metallen ein vergleichsweise hohen Ausdehnungskoeffizienten besitzt.
    Obwohl das Driftgeschwindigkeiten von Materialien sind, verifizieren sie auch das errechnete Driftverhalten des Gesamtsystems.
    Die Bilder erscheinen oft verwaschen und ohne scharfe Kanten, obwohl eine Struktur erkennbar ist.
    Dies ist auf Fehlerquellen, die in \ref{errors} und \ref{non-linearities} beschrieben werden, zurückzuführen.
    Die statistischen Fehler bei den Gittervektoren sind klein, was dafür spricht, dass etwaige Verzerrungen im Bild gut herausgemittelt werden.
    Der ermittelte Wert von $g_2$ weicht vom Literaturwert \SI{2.46}{\angstrom} um mehrere Standardabweichungen ab, was ebenfalls auf die oben erwähnten Fehlerquellen zurückzuführen ist.
    Der Winkel zwischen den Gittervektoren liegt drei Standradabweichungen vom erwarteten Wert von $\SI{60}{\degree}$.
    In Anbetracht der experimentellen Umstände ist dies akzeptabel.
  \makebibliography
\end{document}
