from labtools import *

R = lt.constant('molar gas constant')
NA = lt.constant('Avogadro constant')
ħ = lt.constant('Planck constant over 2 pi')
kB = lt.constant('Boltzmann constant')

κ = np.loadtxt(lt.file('data/κ'), skiprows=2) * 1e9
V0 = np.loadtxt(lt.file('data/V0'), skiprows=2) * 1e-6
M = np.loadtxt(lt.file('data/M'), skiprows=2)
v_l, v_t = np.loadtxt(lt.file('data/v'), skiprows=2, unpack=True) * 1e3

m = np.loadtxt(lt.file('data/m'), skiprows=2)
U = np.loadtxt(lt.file('data/U'), skiprows=2)
I = np.loadtxt(lt.file('data/I'), skiprows=2) * 1e-3

def R2T(R):
    return scipy.constants.C2K(0.00134 * R**2 + 2.296 * R - 243.02)

t, R_T = np.loadtxt(lt.file('data/debye'), skiprows=2, unpack=True)
t = 900 - t
T = R2T(R_T)
Tf = T[1:]

T_α, α_T = np.loadtxt(lt.file('data/α'), skiprows=2, unpack=True)
α_T *= 1e-6
α = np.interp(Tf, T_α, α_T)

C_p = U * I * t[1:] / np.ediff1d(T) / (m / M)

C_V = C_p - 9 * α**2 * κ * V0 * Tf

@np.vectorize
def f(T, θ_D):
    a = T / θ_D
    return 9 * R.nominal_value * a**3 * scipy.integrate.quad(lambda x: x**4 * np.exp(x) / (np.exp(x) - 1)**2, 0, 1 / a)[0]
f.excluded.add(1)

θ_D = lt.curve_fit(f, Tf[Tf <= 170], C_V[Tf <= 170], p0=[350])

θ_D_theor = ħ / kB * (18 * np.pi**2 * NA / (V0 * (1 / v_l**3 + 2 / v_t**3)))**(1 / 3)

tab = lt.SymbolRowTable()
tab.add_si_row('R', R, r'\joule\per\mole\per\kelvin')
tab.add_si_row(r'\hbar', ħ * 1e34, r'\joule\second', exp='e-34')
tab.add_si_row(r'N_{\t{A}}', NA * 1e-23, r'\per\mole', exp='e23')
tab.add_si_row(r'k_{\t{B}}', kB * 1e23, r'\joule\per\kelvin', exp='e-23')
tab.add_hrule()
tab.add_si_row('κ', κ * 1e-9, r'\giga\pascal', places=0)
tab.add_si_row('V_0', V0 * 1e6, r'\cubic\centi\meter', places=2)
tab.add_si_row(r'M', M * 1e3, r'\gram\per\mole', places=3)
tab.add_si_row(r'v_{\t{l}}', v_l * 1e-3, r'\kilo\meter\per\second', places=1)
tab.add_si_row(r'v_{\t{t}}', v_t * 1e-3, r'\kilo\meter\per\second', places=2)
tab.add_hrule()
tab.add_si_row('m', m, r'\kilogram', places=3)
tab.add_si_row('U', U, r'\volt', places=2)
tab.add_si_row('I', I * 1e3, r'\milli\ampere', places=2)
tab.add_hrule()
tab.add_si_row(r'θ_{\t{D}}', θ_D, r'\kelvin')
tab.add_si_row(r'θ_{\t{D}, \t{theor}}', lt.vals(θ_D_theor), r'\kelvin', places=0)
tab.savetable(lt.new('table/constants.tex'))

tab = lt.SymbolColumnTable()
tab.add_si_column('t', t, r'\second', places=0)
tab.add_si_column('R_T', R_T, r'\ohm', places=1)
tab.add_si_column('T', T, r'\kelvin', places=1)
tab.add_si_column('C_V', np.hstack(([float('nan')], C_V)), r'\joule\per\mole\per\kelvin', places=1)
tab.savetable(lt.new('table/data.tex'))

tab = lt.SymbolColumnTable()
tab.add_si_column('T', np.array_split(T_α, 2)[0], r'\kelvin', places=0)
tab.add_si_column('α', np.array_split(α_T, 2)[0] * 1e6, r'\per\kelvin', exp='e-6', places=2)
tab.add_column(lt.VerticalSpace())
tab.add_si_column('T', np.array_split(T_α, 2)[1], r'\kelvin', places=0)
tab.add_si_column('α', np.array_split(α_T, 2)[1] * 1e6, r'\per\kelvin', exp='e-6', places=2)
tab.savetable(lt.new('table/α.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(70, 300, 100)
ax.axhline(y=3 * lt.vals(R), ls='--', c='c', label='$3 R$')
ax.plot(x, f(x, lt.vals(θ_D_theor)), 'k-', label='Theoriekurve')
ax.plot(x, f(x, lt.vals(θ_D)), 'b-', label='Ausgleichskurve')
ax.plot(Tf[Tf <= 170], C_V[Tf <= 170], 'gx', label='Messwerte in Rechnung')
ax.plot(Tf[Tf > 170], C_V[Tf > 170], 'rx', label='Messwerte nicht in Rechnung')
ax.set_xlim(80, 300)
ax.set_ylim(12, 26)
ax.set_xlabel(r'$\silabel{T}{\kelvin}$')
ax.set_ylabel(r'$\silabel{C_V}{\joule\per\mole\per\kelvin}$')
lt.ax_reverse_legend(ax, loc='lower right')
fig.savefig(lt.new('graphic/C_V.pdf'))
