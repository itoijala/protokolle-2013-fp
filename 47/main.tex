\input{header/report.tex}

\addbibresource{lit.bib}

\SetExperimentNumber{47}

\begin{document}
  \maketitlepage{Temperaturabhängigkeit der Molwärme von Festkörpern}{10.06.2013}{24.06.2013}
  \section{Ziel}
    Mit den Versuchsergebnissen soll aus der Temperaturabhängigkeit der Molwärme $C_V$ von Kupfer ihre Debye-Temperatur $θ_{\t{D}}$ bestimmt werden.
  \section{Theorie}
    \subsection{Das Dulong-Petit-Gesetz}
      Das Dulong-Petit-Gesetz liefert eine klassische Aussage über die Molwärme und folgt aus dem Äquipartitionstheorem.
      Dieses besagt, dass die Wärmeenergie $\avg{u}$ in einem Körper gleichmäßig auf seine Bewegungsfreiheitsgrade aufgeteilt wird und im Mittel
      \begin{eqn}
        \avg{u} = \frac{f}{2} k_{\t{B}} T
      \end{eqn}
      beträgt, wobei $f$ die Anzahl der Freiheitsgrade pro Atom, $k_{\t{B}}$ die Boltzmann-Konstante und $T$ die Temperatur sind.

      In einem Gitter können die Atome Schwingungen in drei senkrecht aufeinander stehenden Richtungen um ihre Gleichgewichtslagen durchführen.
      Diese Schwingungen werden in harmonischer Näherung durch harmonische Oszillatoren beschrieben, für deren kinetische Energie $E_{\t{kin}}$ und potentielle Energie $E_{\t{pot}}$ gilt
      \begin{eqn}
        \avg{E_{\t{kin}}} = \avg{E_{\t{pot}}} .
      \end{eqn}
      Daher folgt in diesem Fall insgesamt
      \begin{eqn}
        \avg{u} = 3 k_{\t{B}} T
      \end{eqn}
      pro Atom und
      \begin{eqn}
        U = 3 k_{\t{B}} N_{\t{A}} T = 3 R T
      \end{eqn}
      pro Mol, wobei $N_{\t{A}}$ die Avogadro-Konstante und $R$ die allgemeinen Gaskonstante sind.

      Aus der statistischen Physik ergibt sich die Beziehung
      \begin{eqn}
        C_{\t{V}} = \g(\p{T}{U};)_V , 
      \end{eqn}
      woraus sich das Dulong-Petit-Gesetz
      \begin{eqn}
        C_{V, \t{DP}} = 3 R \eqlabel{eqn:dulong-petit}
      \end{eqn}
      folgt.
      Im klassischen Fall zeigt die Molwärme also keine Temperatur- oder Materialabhängigkeit.
      Dieses Verhalten wird in der Realität nur für hinreichend hohe Temperaturen erreicht.
    \subsection{Das Einstein-Modell}
      Bei tieferen Temperaturen muss die Quantisierung der Energie beachtet werden.
      Im Folgenden werden quantenmechanische Oszillatoren betrachtet, deren Energien ganzzahlige Vielfache der Grundzustandsenergie $\hbar ω$ sind, wobei $\hbar$ das Planck'sche Wirkungsquantum ist.
      Das Einstein-Modell geht von der Annahme aus, dass alle harmonischen Oszillatoren, die die schwingenden Atome auf ihren Gitterplätzen modellieren, die gleiche Frequenz $ω$ haben.

      Im thermodynamischen Gleichgewicht ist die Wahrscheinlichkeit, dass ein harmonischer Oszillator die Energie $n \hbar ω$, $n \in \setN$, bei einer Temperatur $T$ besitzt, durch die Boltzmann-Verteilung
      \begin{eqn}
        \f{W}(n) = \exp(-\frac{n \hbar ω}{k_\t{B} T})
      \end{eqn}
      gegeben.
      Für die mittlere Energie der harmonsichen Oszillatoren gilt die Bose-Einstein-Verteilung
      \begin{eqn}
        \avg{u} = \frac{\hbar ω}{\exp.\frac{\hbar ω}{k_\t{B} T}.- 1} < k_\t{B} T . \eqlabel{eqn:bose-einstein}
      \end{eqn}
      Für die Molwärme des Einsteinmodells gilt daher
      \begin{eqn}
        C_{V, \t{ES}} = \d{T}; 3 N_{\t{A}} \frac{h ω}{\exp.\frac{\hbar ω}{k_\t{B} T}. - 1} = 3R \frac{\hbar^2 ω^2}{k_\t{B}^2} \frac{1}{T^2} \frac{\exp.\frac{\hbar ω}{k_\t{B} T}.}{\g(\exp.\frac{\hbar ω}{k_\t{B} T}. - 1)^{\! 2}} .
      \end{eqn}
      Sie geht für $k_\t{B} T \gg \hbar ω$
      \begin{eqn}
        \lim_{T \rightarrow \infty} C_{V, \t{ES}} = 3 R
      \end{eqn}
      in das Dulong-Petit-Gesetz \eqref{eqn:dulong-petit} über.
      Die Temperaturabhängigkeit dieses Modells stimmt oberhalb der Debye-Temperatur $θ_{\t{D}}$ mit dem Experiment überein.
      Unterhalb dieser Temperatur reicht die Näherung monofrequenter Schwingungen im Kristall nicht mehr aus und das Modell weicht von der Realität ab.
    \subsection{Das Debye-Modell}
      Für sehr tiefe Temperaturen müssen die Frequenzen durch eine Spektralverteilung $\f{Z}(ω)$ der Eigenschwingungen aller Oszillatoren beschrieben werden.
      Die Molwärme
      \begin{eqn}
        C_{V, \t{DB}} = \d{T}; \int_0^{ω_{\t{c}}} \dif{ω} \f{Z}(ω) \frac{\hbar ω}{\exp.\frac{\hbar ω}{k_\t{B} T}. - 1} , \eqlabel{eqn:c_db_f}
      \end{eqn}
      mit einer Cut-off-Frequenz $ω_\t{c}$ folgt aus \eqref{eqn:bose-einstein}.

      Da $\f{Z}(ω)$, insbesondere für anisotropes und stark dispersives, elastisches Verhalten des Kristalls sehr kompliziert wird, geht die Debye-Näherung von einer Phasengeschwindigkeit aus, bei der die elastische Welle nicht von ihrer Frequenz und Ausbreitungsrichtung im Kristall abhängt.
      Dann lässt sich die Spektralverteilung durch Abzählen der Eigenschwingungen in einem Kristall der Kantenlänge $L$ aus einem infinitesimalen Frequenzintervall $\dif{ω}$ leicht bestimmen.
      Mit dieser Näherung folgt
      \begin{eqn}
        \f{Z}(ω) \dif{ω} = \frac{L^3}{2 \PI^2} ω^2 \g(\frac{1}{v_\t{l}^3} + \frac{2}{v_\t{t}^3}) \dif{ω} , \eqlabel{eqn:simpleZ(ω)}
      \end{eqn}
      wobei $v_{\t{l}}$ und $v_{\t{t}}$ die longitudinale und transversale Phasengeschwindigkeit im Kristall sind.

      Die obere Cut-off-Frequenz, die ein endlicher Kristall aus $N_{\t{A}}$ Atomen mit $3 N_{\t{A}}$ Eigenschwingungen erreichen kann, ist definiert durch
      \begin{eqn}
        \int_0^{ω_\t{D}} \dif{ω} \f{Z}(ω) = 3 N_\t{A} \eqlabel{eqn:def_debye}
      \end{eqn}
      und wird als Debye-Frequenz bezeichnet.
      Mit \eqref{eqn:simpleZ(ω)} und \eqref{eqn:def_debye} folgt
      \begin{eqn}
        ω_{\t{D}} = \frac{18 \PI^2 N_{\t{A}}}{L^3} \g(\frac{1}{v_{\t{l}}^3} + \frac{2}{v_{\t{t}}^3})^{\!\! -1} , \eqlabel{eqn:ω_D}
      \end{eqn}
      sodass
      \begin{eqn}
        \f{Z}(ω) \dif{ω} = \frac{9 N_{\t{A}}}{ω_{\t{D}}^3} ω^2 \dif{ω} .
      \end{eqn}
      Daher gilt für \eqref{eqn:c_db_f}
      \begin{eqns}
        C_{V, \t{DB}} &=& \d{T}; \frac{9 N_{\t{A}} \hbar}{ω_{\t{D}}^3} \int_0^{ω_{\t{D}}} \dif{ω} \frac{ω^3}{\exp.\frac{\hbar ω}{k_{\t{B}} T}. - 1} \\
                      &=& 9 R \g(\frac{T}{θ_{\t{D}}})^{\!\!\! 3} \int_0^{\frac{θ_{\t{D}}}{T}} \dif{x} \frac{x^4 \E^x}{\g(\E^x - 1)^2} , \eqlabel{eqn:C_V_DB}
      \end{eqns}
      mit den Abkürzungen
      \begin{eqn}
        x \equiv \frac{\hbar ω}{k_{\t{B}} T} \quad \t{und} \quad \frac{θ_\t{D}}{T} \equiv \frac{\hbar ω_\t{D}}{k_\t{B} T} .
      \end{eqn}

      Die Debye-Temperatur $θ_{\t{D}}$ ist durch individuelle Kristalleigenschaften festgelegt.
      Im Gegensatz dazu ist $C_V = \f{C_V}(θ_{\t{D}} / T)$ eine universelle Funktion, die nicht mehr vom untersuchten Festkörper abhängt.

      Das Integral in \eqref{eqn:C_V_DB} ist für tiefe Temperaturen temperaturunabhängig, sodass für tiefe Temperaturen gilt
      \begin{eqn}
        C_V^{\t{DB}} \propto T^3 .
      \end{eqn}
      Auch die Wärmekapazität der Debye-Theorie geht für große Temperaturen in das Dulong-Petit-Gesetz
      \begin{eqn}
        \lim_{T \to \infty} C_{V, \t{DB}} = 3 R
      \end{eqn}
      über.

      Obwohl die Debye-Theorie für tiefe Temperaturen gut mit experimentellen Daten übereinstimmt, ergibt sich die exakte Molwärme erst für ein genaues Frequenzspektrum $\f{Z}(ω)$, das die Dispersionsrelationen von Longitudinal- und Transversalwellen berücksichtigt.

      Des Weiteren spielen die Leitungselektronen erst bei sehr tiefen Temperaturen eine Rolle, für die die Molwärme der Ionenrümpfe praktisch verschwunden ist.
      Wegen der Fermi-Dirac-Statistik ist dieser Effekt proportional zu $T$.

      Für die Auswertung erweist sich im Folgenden die Relation
      \begin{eqn}
        C_p - C_V = 9 α^2 κ V_0 T , \eqlabel{eqn:C_p-C_V}
      \end{eqn}
      mit der Molwärme $C_p$ bei konstantem Druck, dem linearen Ausdehnungskoeffizienten $α$, dem Kompressionsmodul $κ$ und dem Molvolumen $V_0$, als nützlich, da $C_p$ anstatt $C_V$ gemessen wird.
  \section{Aufbau und Durchführung}
    Der schematische Aufbau ist durch Abbildung \ref{fig:aufbau} gegeben.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/aufbau.pdf}
      \caption{Versuchsaufbau \cite{anleitung47}}
      \label{fig:aufbau}
    \end{figure}

    Am Anfang wird der Rezipient evakuiert und mit Helium bis Luftdruck gefüllt.
    In das Dewar-Gefäß wird flüssiger Stickstoff bis kurz unter den Rand gefüllt, sodass der Rezipient auf circa $\SI{80}{\kelvin}$ abgekühlt wird.
    Wenn diese Temperatur erreicht ist, wird der Rezipient wieder evakuiert, sodass der Luftdruck auf einen möglichst geringen Wert fällt; somit werden Energieverluste durch Konvektion und Wärmeleitung, die durch die Molekularbewegung des Heliums hervorgerufen wird, verhindert.
    Eine merkliche Druckveringerung kann dadurch erreicht werden, dass das Rezipientengefäß ständig auf Stickstofftemperatur gehalten wird.

    Bei der Messung wird der abgekühlten Probe über eine in ihrem Innern angebrachte Heizwicklung eine definierte elektrische Energie $E$ zugeführt und die dabei auftretende Temperaturerhöhung $\Del{T}$ gemessen.
    Die Energie wird durch Messen der Spannung $U$, der Stromstärke $I$ und der Zeit $t$ bestimmt.
    Die Messpunkte werden bei Temperaturdifferenzen von circa \SIrange{7}{11}{\kelvin} aufgenommen.

    Durch Gleichhalten der Temperatur von Probe und Kupfergefäß werden Strahlungsverluste ausgeglichen.
    Dies geschieht mit Heizwicklungen im Kupfer-Zylinder, der den Rezipienten umhüllt.
    Für die Temperaturmessung sind $\ce{Pt}$\--$100$-Wi\-der\-stän\-de am Kupferzylinder und der Probe angebracht; ihr Widerstand wird mit digitalen Ohmmetern gemessen und ist eine monotone Funktion der Temperatur.

  \section{Messwerte und Auswertung}
    Der Zusammenhang zwischen Temperatur $T$ und Widerstand $R_T$ ist durch die Funktion
    \begin{eqn}
      T = \SI{0.00134}{\degreeCelsius\per\ohm\squared} R_T^2 + \SI{2.296}{\degreeCelsius\per\ohm} R_T - \SI{243.02}{\degreeCelsius}
    \end{eqn}
    gegeben.
    Aus der Masse $m$ der Probe, ihrem Molekulargewicht $M$, sowie $\Del{T}$ und $E$ kann
    \begin{eqn}
      C_p = \frac{E}{\Del{T}} \frac{M}{m} = \frac{U I t}{\Del{t}} \frac{M}{m}
    \end{eqn}
    berechnet werden.
    Die Molwärme $C_V$ wird über Formel \eqref{eqn:C_p-C_V} berechnet, wobei $α$ durch lineare Interpolation der Werte aus Tabelle \ref{tab:α} gewonnen wird.
    Die Debye-Temperatur wird durch eine Ausgleichsrechnung der Funktion $\f{C_V}(θ_{\t{D}} / T)$ aus Formel \eqref{eqn:C_V_DB} berechnet.
    Dabei wird das Integral numerisch mittels der Integrationsroutine \texttt{scipy.integrate.quad} \cite{scipy} ausgewertet.
    Zusätzlich werden nur Messpunkte mit $T < 170$ berücksichtigt, da die Messwerte ab dieser Temperatur stark streuen.
    Der Theoriewert $θ_{\t{D}, \t{theor}}$ der Debye-Temperatur wird über die Formel \eqref{eqn:ω_D} berechnet, wobei
    \begin{eqn}
      θ_{\t{D}} = \frac{\hbar}{k_{\t{B}}} ω_{\t{D}}
    \end{eqn}
    ist.
    Tabelle \ref{tab:data} enthält die Messwerte der Zeit $t$ und des Widerstands $R_T$ und die berechneten Werte für die Temperatur $T$ und die Molwärme $C_V$.
    Tabelle \ref{tab:constants} enthält die verwendeten Werte der Naturkonstanten \cite{scipy} $R$, $\hbar$, $N_{\t{A}}$ und $k_{\t{B}}$, die Materialeigenschaften von Kupfer $κ$ \cite{ledbetter-naimon}, $V_0$ \cite{white-collocott}, $M$ \cite{nist-chemistry-webbook}, $v_{\t{l}}$ \cite{anleitung47} und $v_{\t{t}}$ \cite{anleitung47}, die Werte des Masse $m$, der Spannung $U$ und des Stroms $I$ und die Ergebnisse für die Debye-Temperatur.
    Abbildung \ref{fig:C_V} zeigt die Messwerte der Molwärme in Abhängigkeit von der Temperatur, die Ausgleichskurve und die Theoriekurve.
    \begin{table}
      \input{table/α.tex}
      \caption{Zusammenhang zwischen Temperatur $T$ und linearem Ausdehnungskoeefizient $α$ \cite{anleitung47}}
      \label{tab:α}
    \end{table}
    \begin{table}
      \input{table/data.tex}
      \caption{Messwerte und berechnete Werte}
      \label{tab:data}
    \end{table}
    \begin{table}
      \input{table/constants.tex}
      \caption{Natur- und Materialkonstanten, Debye-Temperatur}
      \label{tab:constants}
    \end{table}
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/C_V.pdf}
      \caption{Messwerte, Ausgleichskurve und Theoriekurve}
      \label{fig:C_V}
    \end{figure}
  \section{Diskussion}
    Bei der Messung ergab sich das Problem, dass die Erwärmung des Kupferzylinders nicht immer synchron mit der Erwärmung der Probe eingestellt werden konnte.

    Der experimentell ermittelte Wert für die Debye-Temperatur weicht um 4 Standardabweichungen von der Theorie ab.
    Dies kann einerseits an der Näherung der Spektralverteilung bei der theoretischen Berechnung, andererseits an dem im letzten Satz beschriebenen Messproblem liegen.
  \makebibliography
\end{document}
