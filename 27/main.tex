\input{header/report.tex}

\addbibresource{lit.bib}

\SetExperimentNumber{27}

\begin{document}
  \maketitlepage{Der Zeeman-Effekt}{13.02.2013}{20.02.2013}
  \section{Ziel}
    Ziel des Versuchs ist es, die als Zeeman-Effekt bezeichnete Aufspaltung der Linien im Emissionsspektrum eines Atoms in einem Magnetfeld zu messen, daraus die Landé-Faktoren zu bestimmen und diese mit Theoriewerten zu vergleichen.
  \section{Theorie}
    \subsection{Drehimpulsquantenzahlen}
      \label{drehimpulse}
      In der Quantenmechanik gilt für einen Drehimpuls $\v{j}$
      \begin{eqns}
        \v{j}^2 \ket{j, m_j} &=& \hbar^2 j \g(j + 1) \ket{j, m_j} \\
        j_z \ket{j, m_j} &=& \hbar m_j \ket{j, m_j} ,
      \end{eqns}
      wobei $\hbar$ die reduzierte Planckkonstante ist und $-j \le m_j \le j$ gilt.
      Für den Drehimpuls $\v{j}$ kann der Betrag
      \begin{eqn}
        \abs{\v{j}} = \hbar \sqrt{j \g(j + 1)}
      \end{eqn}
      definiert werden.

      Elektronen in einer Atomhülle besitzen sowohl einen Bahndrehimpuls $\v{l}$ als auch einen Spin $\v{s}$.
      Der Spin jedes Elektrons ist $s = 1/2$ und es gilt $l \le n - 1$ mit der Hauptquantenzahl $n$.
      Der Zustand eines Elektrons wird durch $\ket{n, l, m_l, s, m_s}$ beschrieben.
    \subsection{Das magnetische Moment eines Elektrons}
      Mit dem Korrespondenzprinzip lässt sich aus der klassischen Definition des magnetischen Momentes
      \begin{eqn}
        \v{m} = - \nabla_{\! \v{B}} W
      \end{eqn}
      der Operator des magnetischen Momentes
      \begin{eqn}
        \v{μ} = - \nabla_{\! \v{B}} \mathcal{H} \eqlabel{eqn:Op_magn-Moment}
      \end{eqn}
      herleiten, wobei $W$ die Systemenergie, $\v{B}$ das Magnetfeld und $\mathcal{H}$ der Hamiltonoperator des Systems sind.
      In vereinfachter Form lautet der Hamiltonoperator für ein Ein-Elektronatom im Magnetfeld
      \begin{eqn}
        \mathcal{H} = \mathcal{H}_{\! 0} + \frac{e}{2 m_{\t{e}}} \v{l} \cdot \v{B} ,
      \end{eqn}
      wobei in $\mathcal{H}_{\! 0}$ alle vom Magnetfeld unabhängigen Terme zusammengefasst sind.
      Mit \eqref{eqn:Op_magn-Moment} folgt
      \begin{eqn}
        \v{μ}_{\v{l}} = - \frac{e}{2 m_{\t{e}}} \v{l} ,
      \end{eqn}
      wobei $e$ die Elementarladung und $m_{\t{e}}$ die Elektronenmasse sind.
      Ein Elektron mit $l = 1$ und $m_l = 1$ besitzt das elementare magnetische Moment
      \begin{eqn}
        μ_{\t{B}} = - \frac{e \hbar}{2 m_{\t{e}}} ,
      \end{eqn}
      das als bohrsches Magneton bezeichnet wird.
      Für den Betrag des magnetischen Momentes gilt also im Allgemeinen
      \begin{eqn}
        μ_l = - μ_{\t{B}} \sqrt{l (l + 1)} ,
      \end{eqn}
      wobei die Richtung durch die Richtung von $\v{l}$ gegeben ist.

      Die Einführung eines magnetischen Momentes für den Spin war Folge des Stern-Gerlach-Experiments.
      Die Herleitung des magnetischen Momentes für den relativistischen Spin aus der Dirac-Gleichung resultiert in
      \begin{eqn}
        \v{μ}_{\v{s}} = - g_s \frac{μ_{\t{B}}}{\hbar} \v{s} .
      \end{eqn}
      Der Faktor $g_s$ wird als Landé-Faktor bezeichnet und besitzt den Wert $g_s \approx 2$, welcher ebenfalls aus der diracschen Theorie und der Quantenelektrodynamik folgt.
      Für den Betrag gilt
      \begin{eqn}
        μ_s = - g_s μ_{\t{B}} \sqrt{s \g(s + 1)} .
      \end{eqn}
      Die Tatsache, dass der Betrag des magnetischen Momentes des Spins für ein Elektron doppelt so groß wie $μ_{\t{B}}$ ist, wird als magnetomechanische Anomalie des Elektrons bezeichnet.
    \subsection{Wechselwirkung der Drehimpulse mehrerer Elektronen}
      Da sich in einer Atomhülle mehrere Elektronen befinden, muss die Wechselwirkung ihrer Drehimpulse betrachtet werden.
      Hierbei wird zwischen den Grenzfällen von Atomen niedriger und hoher Ordnungszahl unterschieden.
      Bei Atomen mittlerer Ordnungszahl findet ein fließender Übergang zwischen den Grenzfällen statt.
      Für die Herleitung des Zeeman-Effekts wird im Folgenden der Grenzfall niedriger Ordnungszahl verwendet.
      \subsubsection{Atome niedriger Ordnungszahl}
        Bei Atomen mit niedriger Kernladungszahl ist die Wechselwirkung zwischen den Bahndrehimpulsen $\v{l}_i$ so groß, dass sich diese vektoriell zum Gesamtdrehimpuls
        \begin{eqn}
          \v{L} = \sum \v{l}_i
        \end{eqn}
        zusammensetzen.
        Analog setzt sich der Gesamtspin
        \begin{eqn}
          \v{S} = \sum \v{s}_i
        \end{eqn}
        aus den Spins $\v{s}_i$ der einzelnen Elektronen zusammen.
        Sind die Atome keinen zu hohen Magnetfeldern unterworfen, setzen sich der Gesamtbahndrehimpuls und der Gesamtspin zum Gesamtdrehimpuls $\v{J} = \v{L} + \v{S}$ zusammen.
        Quantenmechanisch betrachtet erfordert diese so genannte $L$-$S$-Kopplung der Drehimpulse eine Drehimpulsaddition von Spin und Bahndrehimpuls.
        Die Gesamtdrehimpulse $\v{L}$, $\v{S}$ und $\v{J}$ folgen auch der in Abschnitt \ref{drehimpulse} beschriebenen Algebra.
        Für die Energieniveaus der Elektronen gilt die Nomenklatur
        \begin{eqn}
          ^M L_J
        \end{eqn}
        mit der Multiplizität $M = 2 S + 1$.
        Des Weiteren werden die ganzzahligen Eigenwerte $L$ des Bahndrehimpulsoperators $\v{L}$ als S- ($L = 0$), P- ($L = 1$), D- ($L = 2$) und F-Terme ($L = 3$) bezeichnet.
      \subsubsection{Atome hoher Ordnungszahl}
        Der zweite Grenzfall tritt bei schweren Atomen auf und wird als $j$-$j$-Kopplung bezeichnet.
        Hier ist die Wechselwirkung zwischen Spin und Bahndrehimpuls des Einzelelektrons groß gegenüber der Wechselwirkung der Einzelbahndrehimpulse oder Einzelspins untereinander.
        Daher setzen sich schon bei den einzelnen Elektronen der Drehimpuls und der Spin zum Gesamtdrehimpuls $\v{j}_i = \v{l}_i + \v{s}_i$ zusammen.
        Diese koppeln dann zum Gesamtdrehimpuls
        \begin{eqn}
          \v{J} = \sum \v{j}_i
        \end{eqn}
        der Hülle.
    \subsection{Aufspaltung der Energieniveaus eines Atoms im homogenen Magnetfeld}
      Für das gesamte magnetischen Moment der Hülle gilt
      \begin{eqn}
        \v{μ} = \v{μ}_L + \v{μ}_S .
      \end{eqn}
      Die Richtungen dieses Momentes und des Gesamtdrehimpulses $\v{J}$ fallen im Allgemeinen nicht zusammen.
      Deshalb gibt es auf den Gesamtdrehimpuls bezogene senkrechte und parallele Anteile.
      Aus der Quantenmechanik folgt, dass der Erwartungswert der senkrechten Komponente verschwindet; klassisch gesehen verschwindet der senkrechte Anteil, da das Moment eine Präzessionsbewegung um die Richtung des äußeren Feldes durchführt.
      Insgesamt ergibt sich für den Betrag
      \begin{eqn}
        \abs{\v{μ}} = μ_{\t{B}} g \sqrt{J \g(J + 1)} .
      \end{eqn}
      Für den Landé-Faktor $g$ folgt nach einer kurzen Rechnung
      \begin{eqn}
        g = \f{g}(L, S, J) = \frac{3 J \g(J + 1) + S \g(S + 1) - L \g(L + 1)}{2 J \g(J + 1)} . \eqlabel{eqn:landé_faktor}
      \end{eqn}

      Ein weiterer Effekt der Quantenmechanik ist die Richtungsquantelung.
      Diese besagt in Vektoren ausgedrückt, dass nach Einschalten eines äußeren Magnetfeldes $\v{B} = B \uv{e}_z$ nur solche Winkel zwischen $\v{μ}$ und $\v{B}$ auftreten können, bei denen die Komponente $μ_z$ in Feldrichtung ein ganzzahliges Vielfaches von $g μ_{\t{B}}$ ist, also
      \begin{eqn}
        μ_z = m g μ_{\t{B}} . \eqlabel{eqn:magn-moment}
      \end{eqn}
      Die Größe $m$ ist die dritte Komponente des Gesamtdrehimpulses, analog zu den anderen Drehimpulsen.
      Auch hier gibt es $2 J + 1$ Einstellmöglichkeiten eines atomaren magnetischen Momentes relativ zu einem äußeren Feld.
      Daraus folgt die Zusatzenergie
      \begin{eqn}
        E_{\t{mag}} = - \v{μ} \cdot \v{B} , \eqlabel{eqn:magn-energie}
      \end{eqn}
      die ein Moment im äußeren Feld erhält.
      Mit Gleichung \eqref{eqn:magn-moment} folgt für die Energieänderung des Energieniveaus
      \begin{eqn}
        \Del{E} = μ_{\t{B}} m \f{g}(L, S, J) B . \eqlabel{eqn:energieänderung}
      \end{eqn}
      Dies bedeutet, dass sich das im feldfreien Raum entartete Energieniveau im magnetischen Feld in äquidistante Niveaus aufspaltet, deren Anzahl durch \mbox{$2 J + 1$} gegeben ist.
      Dies ist darauf zurückzuführen, dass das magnetische Feld die Isotropie des Raumes bricht und so zu einer Aufhebung der Entartung führt.
      Die Aufspaltung tritt auch bei angeregten Zuständen auf, daher ist zu erwarten, dass bei eingeschaltetem Feld zusätzliche Übergänge zwischen den neu entstandenen Energieniveaus auftreten.
      Diese Aufspaltung wird als Zeeman-Effekt bezeichnet.
    \subsection{Auswahlregeln für Übergänge}
      Aus den möglichen Übergängen zwischen Energieniveaus treten in der Natur nur bestimmte auf.
      Die Schrödingergleichung
      \begin{eqn}
        \mathcal{H} \f{Ψ}(\v{r}, t) = \frac{\hbar}{\I} \p{t}.\f{Ψ}(\v{r}, t).;
      \end{eqn}
      beschreibt nicht-relativistische Elektronen, wobei
      \begin{eqn}
        \mathcal{H} = \frac{\v{p}^2}{2 m_{\t{e}}} + \f{U}(\v{r}, t)
      \end{eqn}
      der Hamiltonoperator mit dem Potential $U$ ist.
      Um den Spin zu berücksichtigten, ist eine Herleitung der Auswahlregeln aus der relativistischen Dirac-Gleichung notwendig, die aber am Ende das selbe Resultat liefert.
      Die Schrödingergleichung hat die Lösungen
      \begin{eqn}
        \f{Ψ_ξ}(\v{r}, t) = \f{ψ_ξ}(\v{r}) \exp.\frac{E_ξ t}{\I \hbar}. ,
      \end{eqn}
      wobei $E_ξ$ die Energie des Zustands und $\f{ψ_ξ}(\v{r})$ eine Lösung der zeitunabhängigen Schrödingergleichung
      \begin{eqn}
        \mathcal{H} \f{ψ_ξ}(\v{r}) = E_ξ \f{ψ_ξ}(\v{r})
      \end{eqn}
      ist.
      Da die Schrödingergleichung linear ist, ist auch die Linearkombination
      \begin{eqn}
        \f{Ψ_{\t{ges}}}(\v{r}, t) = N_α \f{Ψ_α}(\v{r}, t) + N_β \f{Ψ_β}(\v{r}, t)
      \end{eqn}
      eine Lösung.

      Die Wahrscheinlichkeitsdichte
      \begin{eqn}
        \abs{\f{Ψ_{\t{ges}}}(\v{r}, t)}^2
      \end{eqn}
      ist zeitabhängig und beschreibt Oszillationen der Elektronen zwischen zwei Energieniveaus.
      Dadurch wird ein Dipolmoment $\v{D}$ erzeugt, mit dem die Intensität der emittierten Strahlung durch den Poyntingvektor $\v{S}$ berechnet werden kann.
      Die $x$-Komponente des Dipolmoments lautet
      \begin{eqn}
        D_x = - e \int^{} \dif{V} x \abs{Ψ_{\t{ges}}}^2 .
      \end{eqn}
      Dabei wird das Integral
      \begin{eqn}
        x_{α,β} = \int^{} \dif{V} x \abs{Ψ_{\t{ges}}}^2 \eqlabel{eqn:matrixelement}
      \end{eqn}
      als Matrixelement bezeichnet, das mit den Elementen der anderen Raumrichtungen zur Berechnung des Poyntingvektors
      \begin{eqn}
        \abs{\v{S}_{α, β}} \propto \g(\abs{x_{α,β}}^2 + \abs{y_{α,β}}^2 + \abs{z_{α,β}}^2) \sin^2.γ. \eqlabel{eqn:poynting}
      \end{eqn}
      dient.
      Der Winkel $γ$ ist hierbei der Winkel zwischen dem Dipolmoment und der Ausbreitungsrichtung der elektromagnetischen Strahlung.
      Sei der Zustand eines Elektrons der Hülle im Magnetfeld durch
      \begin{eqn}
        \f{ψ}(r, θ, φ) = \frac{1}{\sqrt{2 \PI}} \f{R}(r) \f{Θ}(θ) \E^{\I m φ} \eqlabel{eqn:zustand}
      \end{eqn}
      gegeben.
      Dabei repräsentiert $\f{R}(r)$ den Ortsanteil und $\f{Θ}(θ)$ den ersten Winkelanteil.
      Der zweite Winkelanteil ist durch die Aufhebung der Entartung durch das Magnetfeld von der Quantenzahl $m$ abhängig.
      Wenn das Magnetfeld in $z$-Richtung orientiert ist, ergibt sich für das Matrixelement mit \eqref{eqn:matrixelement} und \eqref{eqn:zustand} in Kugelkoordinaten
      \begin{eqn}
        z_{α, β} = \frac{1}{2 \PI} \int^{} \dif{r} R_α R_β r^3 \int^{} \dif{\vartheta} Θ_α Θ_β \sin.\vartheta. \cos.\vartheta. \int^{} \dif{φ} \E^{\I \g(m_β - m_α) φ} .
      \end{eqn}
      Das Matrixelement verschwindet für
      \begin{eqn}
        m_α \neq m_β .
      \end{eqn}
      Die anderen Matrixelemente ergeben sich aus analogen Rechnungen.
      Die Matrixelemente $x_{α,β}$ und $y_{α,β}$ verschwinden, wenn
      \begin{eqn}
        m_α \neq m_β \pm 1 .
      \end{eqn}
      Daraus ergeben sich insgesamt die Auswahlregeln 
      \begin{eqn}
        m_α - m_β = \Del{m} \in \g\{0, \pm 1\} .
      \end{eqn}
      Die Übergänge mit $\Del{m} = 0$ werden als $\mathup{π}$-, diejenigen mit $\Del{m} = \pm 1$ als $\mathup{σ}$-Übergänge bezeichnet.
      Für $\Del{m} = 0$ liefert nur das Matrixelement $z_{α, β}$ einen Beitrag zu \eqref{eqn:poynting}; der Dipol schwingt parallel zum Magnetfeld.
      Wegen der Winkelabhängigkeit in \eqref{eqn:poynting} ergibt sich die stärkste Emission senkrecht zum Magnetfeld, das heißt die elektromagnetische Transversalwelle ist linear und parallel zum Magnetfeld polarisert.
      Für $\Del{m} = \pm 1$ gilt
      \begin{eqn}
        x_{α, β} = \E^{\pm \I \frac{\PI}{2}} y_{α, β} ;
      \end{eqn}
      der Dipol schwingt also zirkularpolarisiert um die Richtung des Magnetfeldes.
      Die Übergänge $\Del{m} = 1$ und $\Del{m} = -1$ unterscheiden sich lediglich in der Drehrichtung der Polarisation.

      Wegen der Polarisation der emittierten Strahlung können die Linien nicht aus jeder Richtung erkannt werden.
      Die $\mathup{π}$\-/Komponente ist wie oben erwähnt am besten senkrecht zum Feld sichtbar.
      Die $\mathup{σ}$-Komponenten sind zirkular um die Magnetfeldrichtung polarisiert; senkrecht zum Feld wird also eine lineare Polarisation beobachtet.
    \subsection{Der Zeeman-Effekt}
      Es wird zwischen dem anomalen und dem normalen Zeeman-Effekt unterschieden, wobei der normale ein Spezialfall des anomalen ist.
      \subsubsection{Der anomale Zeeman-Effekt}
        Für die Energiedifferenz ergibt sich aus \eqref{eqn:magn-moment} und \eqref{eqn:magn-energie}
        \begin{eqn}
          \Del{E_{1, 2}} = μ_{\t{B}} B \g(m_1 \f{g}(L_1, S_1, J_1) - m_2 \f{g}(L_2, S_2, J_2)) .
        \end{eqn}
        Da der Landé-Faktor von den Quantenzahlen $L$, $S$ und $J$ abhängt, wird die Aufspaltung linienreicher.
        In Abbildung \ref{fig:3P1} ist das Termschema für $^3 \t{P}_1 \longrightarrow {}^3 \t{S}_1$ aufgezeichnet.
        \begin{figure}
          \input{graphic/3P1.tex}
          \caption{Termschema für $^3 \t{P}_1 \longrightarrow {}^3 \t{S}_1$}
          \label{fig:3P1}
        \end{figure}
      \subsubsection{Der normale Zeeman-Effekt}
        Der normale Zeeman-Effekt ist ein Spezialfall des anomalen für $S = 0$.
        Der Landé-Faktor \eqref{eqn:landé_faktor} ergibt in diesem Fall immer $g = 1$, die Verschiebung der Energieniveaus ist also unabhängig von jeder Drehimpulsquantenzahl.
        Aus \eqref{eqn:energieänderung} folgt
        \begin{eqn}
          \Del{E} = \Del{m} μ_{\t{B}} B ,
        \end{eqn}
        woraus wiederum folgt, dass die Wellenlänge der $\mathup{π}$-Linien vom Magnetfeld nicht beeinflusst wird.
        Der Abstand der Energieniveaus ist für die Übergänge mit gleichem $\Del{m}$ beim normalen Zeeman-Effekt gleich.
        Die Aufspaltung der Spektrallinien in diese drei Komponenten wird als Zeeman-Triplett bezeichnet.
        In Abildung \ref{fig:1D2} ist das Termschema für $^1 \t{D}_2 \longrightarrow {}^1 \t{P}_1$ aufgezeichnet.
        \begin{figure}
          \input{graphic/1D2.tex}
          \caption{Termschema für $^1 \t{D}_2 \longrightarrow {}^1 \t{P}_1$}
          \label{fig:1D2}
        \end{figure}
  \section{Aufbau}
    Eine graphische Darstellung des Aufbaus befindet sich in Abbildung \ref{fig:aufbau}.
    Für die Untersuchung des normalen und anomalen Zeeman-Effekts wird eine $\ce{Cd}$-Lampe zwischen die Polschuhe eines Elektromagneten gestellt.
    Transversal zum Magnetfeld wird das emittierte Licht kollimiert und hinsichtlich der Wellenlänge in einem Glasprisma separiert.
    Der Übergang, der untersucht wird, wird durch einen Polarisationsfilter und einen Spalt nach Polarisationsrichtung und Wellenlänge ausgewählt und schließlich scharf auf die Eintrittsfläche einer Lummer\-/Gehrcke\-/Platte abgebildet.
    Das Interferenzmuster, das durch die Lummer-Gehrcke-Platte erzeugt wird, wird fotografiert.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/aufbau.pdf}
      \caption{Versuchsaufbau \cite{anleitung27}}
      \label{fig:aufbau}
    \end{figure}

    Durch die Interferenz an planparallelen Grenzflächen erreicht die Lummer\-/Gehrcke\-/Platte ein hohes Auflösungsvermögen in der Größenordnung $\num{e6}$.
    Dabei wird über ein Prisma das parallel einfallende Licht innerhalb der planparallelen Platte reflektiert.
    Bei jeder Reflektion tritt ein geringer Teil der Strahlung aus dem Glas und kann mit anderen Strahlen interferieren.
    Die Bedingung für konstruktive Interferenz hängt von der Dicke $d$ der Platte und der eingestrahlten Wellenlänge $λ$ ab.
    Wird monochromatisches Licht eingestrahlt, so ist der Gangunterschied $s$ der Interferenzstreifen auf der Platte genau $λ$.
    Bei eingeschaltetem Magnetfeld verändert sich die Wellenlänge um $\del{λ}$, sodass es zu einer Verschiebung der Interferenzstreifen um $\del{s}$ kommt.

    Im Dispersionsgebiet dürfen sich zwei Wellenlängen nicht überlagern und somit maximal eine Wellenlängendifferenz von
    \begin{eqn}
      \Del{λ} = \frac{λ^2}{2 d \sqrt{n^2 - 1}} \eqlabel{eqn:dispersionsgebiet}
    \end{eqn}
    aufweisen.
    Für das Auflösungsvermögen der Lummer-Gehrcke-Platte gilt
    \begin{eqn}
      A = \frac{a}{λ} \g(n^2 -1) , \eqlabel{eqn:auflösungsvermögen}
    \end{eqn}
    wobei $a$ die Länge der Platte und $n$ der Brechungsindex sind.
  \section{Durchführung}
    In einem ersten Schritt wird die Strom-Magnetfeld-Kurve aufgenommen.
    Dazu wird eine Hall-Sonde zwischen die Polschuhe des Magneten gehalten, während der Strom durch die Spulen schrittweise erhöht wird.

    Anschließend wird die Optik aus Abbildung \ref{fig:aufbau} justiert.
    Dazu wird das Licht der $\ce{Cd}$-Lampe mit einem Objektiv und der Linse $\mathup{L}_1$ scharf und mittig auf den Spalt $\mathup{S}_1$ abgebildet.
    Hinter dem Spalt wird die Linse $\mathup{L}_2$ so justiert, dass ein möglichst paralleles Lichtbündel auf das Geradsichtprisma $\mathup{GP}$ fällt, dabei darf nur ein möglichst kleiner Teil des Lichtbündels nicht das Prisma treffen, um Intensitätsverluste zu minimieren.
    Mit der Linse $\mathup{L}_3$ wird ein scharfes Bild auf den Spalt $\mathup{S}_2$ abgebildet, mit dem eine Farbe aus dem Spektrum der $\ce{Cd}$-Lampe gewählt werden kann.
    Die Linse $\mathup{L}_4$ dient dazu, ein scharfes Bild des Spaltes auf die Eintrittsfläche der Lummer-Gehrcke-Platte $\mathup{LG}$ abzubilden.
    Um die Aufspaltung bei der gegebenen Auflösung der Lummer-Gehrke-Platte beobachten zu können, können mit einem Polarisatoionsfilter die $\mathup{π}$-Linien bei einer Einstellung von $\SI{0}{\degree}$ und die $\mathup{σ}$-Linien bei einer Einstellung von $\SI{90}{\degree}$ ausgeblendet werden; die letzte Einstellung ist durchlässig für die Wellen, die parallel zur Magnetfeldrichtung polarisiert sind.
    Hinter der Platte wird eine Digitalkamera angebracht.
    Die Platte und die Kamera werden so lange gedreht, bis ein Bild gemacht werden kann, das eine klare Abgrenzung der Linien ohne Magnetfeld ermöglicht.
    Anschließend wird das Magnetfeld eingeschaltet, der verwendete Strom notiert und bei der selben Konfiguration ein zweites Bild aufgenommen.
    Wenn keine Aufspaltung der Linien erkannt wird, wird die Platte nochmal gedreht, bis die Aufspaltung beobachtet werden kann.
    Der Focus der Kamera wird auf den Landschaftmodus eingestellt, da paralleles, also Licht von einer unendlich weit entfernten Quelle, auf die Linse trifft.
    Da sich beim Drehen der Lummer-Gehrcke-Platte ein Intensitätsverlust zu Gunsten besserer Auflösung einstellen kann, muss die Belichtungszeit manuell angepasst werden.
    Bei der roten Spektrallinie handelt es sich um den normalen Zeeman-Effekt; beobachtbar ist hier nur die Aufspaltung in $\mathup{σ}$-Linien.
    Bei der hellblauen Spektrallinie werden Bilder für beide Polarisationsfiltereinstellungen gemacht.
  \section{Messwerte und Auswertung}
    Im Folgenden steht $\mean{x}$ für den Mittelwert der Größe $x$ und $\err{y}$ für den Fehler der Größe $y$.
    \subsection{Magnetfeld}
      Zur Auswertung der Ergebnisse der Magnetfeldmessung wird eine lineare Regression mit der Funktion
      \begin{eqn}
        \f{B}(I) = C I + D \eqlabel{eqn:B}
      \end{eqn}
      durchgeführt.
      Die Messwerte des Stroms $I$ und des Magnetfeldes $B$ und die Werte für die Parameter $C$ und $D$ sind in Tabelle \ref{tab:B} aufgeführt; in Abbildung \ref{fig:B} sind die Messwerte und die Regressionsgerade aufgetragen.
      In der weiteren Auswertung wird das verwendete $B$-Feld über \eqref{eqn:B} berechnet.
      \begin{table}
        \caption{Messwerte und Ergebnisse der linearen Regression für die Eichkurve des $B$-Feldes}
        \label{tab:B}
        \input{table/B.tex}
      \end{table}
      \begin{figure}
        \includegraphics{graphic/B.pdf}
        \caption{Messwerte und Ausgleichsgerade des $B$-Feldes}
        \label{fig:B}
      \end{figure}
      \FloatBarrier
    \subsection{Lummer-Gehrcke-Platte}
      In Tabelle \ref{tab:theory} sind die Parameter der Lummer-Gehrcke-Platte sowie die mit den Formeln \eqref{eqn:dispersionsgebiet} und \eqref{eqn:auflösungsvermögen} berechneten Werte für das Dispersionsgebiet und das Auflösungsvermögen aufgeführt.
      \begin{table}
        \caption{Parameter der Lummer-Gehrcke-Platte}
        \label{tab:theory}
        \input{table/theory.tex}
      \end{table}
      \FloatBarrier
    \subsection{Allgemeines Vorgehen}
      Aus Formel \eqref{eqn:dispersionsgebiet}, der Beziehung
      \begin{eqn}
        \del{λ} = \frac{1}{2} \frac{\del{s}}{\Del{s}} \Del{λ}
      \end{eqn}
      und dem Zusammenhang
      \begin{eqn}
        E = \frac{2 \PI \hbar c}{λ} \eqlabel{eqn:Eλ}
      \end{eqn}
      zwischen Energie und Wellenlänge ergibt sich für die Energieabstände
      \begin{eqn}
        \Del{E} = \frac{\PI \hbar c}{2 d \sqrt{n^2 - 1}} \frac{\del{s}}{\Del{s}} ,
      \end{eqn}
      wobei $c$ die Lichtgeschwindigkeit im Vakuum ist.
      Dabei ist $\Del{s}$ der Abstand zweier Maxima im Bild ohne Magnetfeld und $\del{s}$ der Abstand der zwei zu einer Ordnung gehörenden Linien mit Magnetfeld.

      Zur Bestimmung der Positionen der Linien in den aufgenommenen Bildern wird eine Zeile ausgewählt, die sowohl mit als auch ohne Magnetfeld einen guten Kontrast bietet, wobei das Bild mit $B \neq 0$ einen schlechteren Kontrast hat.
      Die Graustufenwerte dieser Zeile werden als Funktion der $x$-Koordinate betrachtet und aufgetragen.
      Die Maxima dieser Funktionen und ihre Abstände werden bestimmt.
    \subsection{Rote Spektrallinie}
      Die aufgenommenen Bilder befinden sich in Abbildung \ref{fig:red-data}.
      Die ermittelten Maxima und die daraus berechneten Energieänderungen sind in Tabelle \ref{tab:red} aufgeführt.
      Der Graustufenwert der ausgewählten Zeile und die Maxima sind in Abbildung~\ref{fig:red} aufgetragen.
      Für den Theoriewert der Energieänderung gilt
      \begin{eqn}
        \Del{E_{\t{theor}}} = μ_{\t{B}} B .
      \end{eqn}
      Für den Landé-Faktor beim normalen Zeeman-Effekt gilt
      \begin{eqn}
        g = \frac{\mean{\Del{E}}}{μ_{\t{B}} B} .
      \end{eqn}
      Die experimentell ermittelten Werte von $g$ sowie die theoretisch berechneten stehen in Tabelle \ref{tab:g-red}.
      \begin{figure}
        \subfloat[$B = 0$]{\includegraphics[width=0.5\textwidth-0.5em]{data/red-noB.jpg}}
        \quad
        \subfloat[$B \neq 0$]{\includegraphics[width=0.5\textwidth-0.5em]{data/red-B.jpg}}
        \caption{Aufgenommene Bilder der roten Spektrallinie}
        \label{fig:red-data}
      \end{figure}
      \begin{figure}
        \includegraphics{graphic/red.pdf}
        \caption{Intensitätsverteilung der roten Spektrallinien ohne Magnetfeld und $\mathup{σ}$-Linien im Magnetfeld}
        \label{fig:red}
      \end{figure}
      \begin{table}
        \caption{Ermittelte Abstände der Maxima der roten $\mathup{σ}$-Linien mit Magnetfeld, sowie die Aufspaltung $\Del{E}$ und eingesetztes Magnetfeld}
        \label{tab:red}
        \input{table/red.tex}
      \end{table}
      \begin{table}
        \caption{Theoretische und experimentelle Landé-Faktoren}
        \label{tab:g-red}
        \input{table/g-red.tex}
      \end{table}
      \FloatBarrier
    \subsection{Blaue Spektrallinie}
      \subsubsection{\texorpdfstring{$\mathup{π}$}{π}-Übergänge}
        Die aufgenommenen Bilder befinden sich in Abbildung \ref{fig:blue-π-data}.
        Die ermittelten Maxima und die daraus berechneten Energieänderungen sind in Tabelle \ref{tab:blue-π} aufgeführt.
        Der Graustufenwert der ausgewählten Zeile und die Maxima sind in Abbildung~\ref{fig:blue-π} aufgetragen.
        Für den Theoriewert der Energieänderung gilt
        \begin{eqn}
          \Del{E_{\t{theor}}} = μ_{\t{B}} B \g(g_{^3 \t{S}_1} - g_{^3 \t{P}_1}) ,
        \end{eqn}
        wobei die Theoriewerte von $g$ in Tabelle \ref{tab:g-blue} aufgeführt sind.
        \begin{figure}
          \subfloat[$B = 0$]{\includegraphics[width=0.5\textwidth-0.5em]{data/blue-π-noB.jpg}}
          \quad
          \subfloat[$B \neq 0$]{\includegraphics[width=0.5\textwidth-0.5em]{data/blue-π-B.jpg}}
          \caption{Aufgenommene Bilder der $\mathup{π}$-Übergänge der blauen Spektrallinie}
          \label{fig:blue-π-data}
        \end{figure}
        \begin{figure}
          \includegraphics{graphic/blue-π.pdf}
          \caption{Intensitätsverteilung der blauen Spektrallinien ohne Magnetfeld und $\mathup{π}$-Linien im Magnetfeld}
          \label{fig:blue-π}
        \end{figure}
        \begin{table}
          \caption{Ermittelte Abstände der Maxima der hellblauen $\mathup{π}$-Linien mit Magnetfeld, sowie die Aufspaltung $\Del{E}$ und eingesetztes Magnetfeld}
          \label{tab:blue-π}
          \input{table/blue-π.tex}
        \end{table}
        \FloatBarrier
      \subsubsection{\texorpdfstring{$\mathup{σ}$}{σ}-Übergänge}
        Die aufgenommenen Bilder befinden sich in Abbildung \ref{fig:blue-σ-data}.
        Die ermittelten Maxima und die daraus berechneten Energieänderungen sind in Tabelle \ref{tab:blue-σ} aufgeführt.
        Der Graustufenwert der ausgewählten Zeile und die Maxima sind in Abbildung~\ref{fig:blue-σ} aufgetragen.
        Für den Theoriewert der Energieänderung gilt
        \begin{eqn}
          \Del{E_{\t{theor}}} = μ_{\t{B}} B g_{^3 \t{P}_1} .
        \end{eqn}
        \begin{figure}
          \subfloat[$B = 0$]{\includegraphics[width=0.5\textwidth-0.5em]{data/blue-σ-noB.jpg}}
          \quad
          \subfloat[$B \neq 0$]{\includegraphics[width=0.5\textwidth-0.5em]{data/blue-σ-B.jpg}}
          \caption{Aufgenommene Bilder der $\mathup{σ}$-Übergänge der blauen Spektrallinie}
          \label{fig:blue-σ-data}
        \end{figure}
        \begin{figure}
          \includegraphics{graphic/blue-σ.pdf}
          \caption{Intensitätsverteilung der blauen Spektrallinien ohne Magnetfeld und $\mathup{σ}$-Linien im Magnetfeld}
          \label{fig:blue-σ}
        \end{figure}
        \begin{table}
          \caption{Ermittelte Abstände der Maxima der hellblauen $\mathup{σ}$-Linien mit Magnetfeld und eingesetztes Magnetfeld}
          \label{tab:blue-σ}
          \input{table/blue-σ.tex}
        \end{table}
        \FloatBarrier

      Für die Landé-Faktoren gilt
      \begin{eqns}
        g_{^3 \t{S}_1} &=& \frac{\mean{\Del{E_{\mathup{σ}}}}}{μ_{\t{B}} B_{\mathup{σ}}} + \frac{\mean{\Del{E_{\mathup{π}}}}}{μ_{\t{B}} B_{\mathup{π}}} \\
        g_{^3 \t{P}_1} &=& \frac{\mean{\Del{E_{\mathup{σ}}}}}{μ_{\t{B}} B_{\mathup{σ}}} .
      \end{eqns}
      Die experimentell ermittelten Werte von $g$ sowie die theoretisch berechneten stehen in Tabelle \ref{tab:g-blue}.
      \begin{table}
        \caption{Theoretische und experimentelle Landé-Faktoren}
        \label{tab:g-blue}
        \input{table/g-blue.tex}
      \end{table}
  \section{Diskussion}
    Das Magnetfeld weist, wie erwartet, eine gute lineare Abhängigkeit vom Strom auf, was aus Abbildung \ref{fig:B} ersichtlich wird.

    Der Vergleich der experimentellen und theoretischen Landé-Faktoren der blauen Linie in Tabelle \ref{tab:g-blue} liefert eine Übereinstimmung mit einer Abweichung zwischen ein und zwei Standardabweichungen.
    Somit kann die Theorie im Rahmen der Messgenauigkeit bestätigt werden.

    Bei der roten Linie in Tabelle \ref{tab:g-red} ist die Abweichung etwas größer.
    Beim Experiment gab es erheblich mehr Schwierigkeiten bei der roten als bei der blauen Linie, was die größere Abweichung erklären könnte.
    Die Abweichung ist aber nicht so groß, dass die Theorie in statistisch signifikanter Weise angezweifelt werden könnte.
  \nocite{nolting52}
  \makebibliography
\end{document}
