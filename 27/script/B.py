from labtools import *

I, B = np.loadtxt(lt.file('data/B'), unpack=True)
B *= 1e-3
I = I[:31]
B = B[:31]

C, D = lt.linregress(I, B)

np.savetxt(lt.new('result/B'), [[lt.vals(C), lt.stds(C)], [lt.vals(D), lt.stds(D)]])

tab = lt.SymbolColumnTable()
tab.add_si_column('I', I, r'\ampere', places=1)
tab.add_si_column('B', B * 1e3, r'\milli\tesla', places=0)

tab2 = lt.SymbolRowTable()
tab2.add_si_row('C', C, r'\tesla\per\ampere')
tab2.add_si_row('D', D, r'\tesla')

tab3 = lt.Table()
tab3.add(lt.Row())
tab3.add_hrule()
tab3.add(lt.Row())
tab3.add(lt.Column([tab, tab2], "@{}c@{}"))
tab3.savetable(lt.new('table/B.tex'))

x = np.linspace(0, 16, 1000)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, lt.vals(C * x + D), 'b-', label='Ausgleichsgerade')
ax.plot(I, B, 'rx', label='Messwerte')
ax.set_xlabel(r'\silabel{I}{\ampere}')
ax.set_ylabel(r'\silabel{B}{\tesla}')
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(1))
lt.ax_reverse_legend(ax, 'upper left')
fig.savefig(lt.new('graphic/B.pdf'))
