from labtools import *

a = np.loadtxt(lt.file('data/a')) * 1e-3
d = np.loadtxt(lt.file('data/d')) * 1e-3
λ = np.loadtxt(lt.file('data/λ')) * 1e-9
n = np.loadtxt(lt.file('data/n'))

Δλ = λ**2 / (2 * d * np.sqrt(n**2 - 1))

A = a / λ * (n**2 - 1)

μ_B = lt.constant('Bohr magneton')
ħ = lt.constant('Planck constant over 2 pi')
c = lt.constant('speed of light in vacuum')

tab1 = lt.SymbolRowTable()
tab1.add_si_row('a', a * 1e3, r'\milli\meter', places=0)
tab1.add_si_row('d', d * 1e3, r'\milli\meter', places=0)

tab2 = lt.SymbolColumnTable()
tab2.add_column(lt.Column([lt.Cell(['rot', 'blau'])], 'l'))
tab2.add_si_column('λ', λ * 1e9, r'\nano\meter', places=1)
tab2.add_si_column('n', n, places=4)
tab2.add_si_column(r'\Del{λ}', Δλ * 1e12, r'\pico\meter', figures=3)
tab2.add_si_column('A', A * 1e-6, exp='e6', figures=3)

tab = lt.Table()
tab.add(lt.Row())
tab.add_hrule()
tab.add(lt.Row())
tab.add(lt.Column([tab1, tab2], "@{}c@{}"))
tab.savetable(lt.new('table/theory.tex'))
