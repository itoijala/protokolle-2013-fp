from labtools import *
import scipy.ndimage

ħ = lt.constant('Planck constant over 2 pi')
c = lt.constant('speed of light in vacuum')
μ_B = lt.constant('Bohr magneton')

d = np.loadtxt(lt.file('data/d')) * 1e-3
n = np.loadtxt(lt.file('data/n'))[1]

L = np.array([1, 2, 0, 1])
M = np.array([1, 1, 3, 3])
J = np.array([1, 2, 1, 1])

S = (M - 1) / 2
g = (3 * J * (J + 1) + S * (S + 1) - L * (L + 1)) / (2 * J * (J + 1))

C, D = np.loadtxt(lt.file('result/B'))
C = unc.ufloat(C[0], C[1])
D = unc.ufloat(D[0], D[1])

# lt.file('data/red-I') lt.file('data/red-noB.jpg') lt.file('data/red-B.jpg')
# lt.new('table/red.tex') lt.new('graphic/red.pdf')
# lt.file('data/blue-π-I') lt.file('data/blue-π-noB.jpg') lt.file('data/blue-π-B.jpg')
# lt.new('table/blue-π.tex') lt.new('graphic/blue-π.pdf')
# lt.file('data/blue-σ-I') lt.file('data/blue-σ-noB.jpg') lt.file('data/blue-σ-B.jpg')
# lt.new('table/blue-σ.tex') lt.new('graphic/blue-σ.pdf')

B = [0] * 3
ΔE_mean = [0] * 3

for i, name, y, x_min, x_max_noB, x_max_B, ΔE_theory in [[0, 'red',    1350, 465, 1370, 1330, lambda B: μ_B * B],
                                                         [1, 'blue-π',  480, 365, 1450, 1430, lambda B: μ_B * B * (g[2] - g[3])],
                                                         [2, 'blue-σ',  530, 465, 1595, 1550, lambda B: μ_B * B * g[3]]]:
    I = np.loadtxt(lt.file('data/' + name + '-I' ))
    I = unc.ufloat(I, 0.1)

    B[i] = C * I + D

    image_noB = scipy.ndimage.imread(lt.file('data/' + name + '-noB.jpg' ), flatten=True)
    image_B = scipy.ndimage.imread(lt.file('data/' + name + '-B.jpg' ), flatten=True)

    row_noB = image_noB[y,x_min:x_max_noB]
    row_B = image_B[y,x_min:x_max_B]

    peaks_noB = np.array(lt.peakdetect(row_noB, None, 10)[0]).T
    peaks_B = np.array(lt.peakdetect(row_B, None, 6)[0]).T

    x_noB = peaks_noB[0]
    x_B = peaks_B[0]

    Δs = np.ediff1d(x_noB)
    δs = np.diff(np.split(x_B, len(x_B) // 2)).flatten()

    ΔE = np.pi * ħ * c/ (2 * d * np.sqrt(n**2 - 1)) * δs / Δs
    ΔE_mean[i] = lt.mean(ΔE)

    tab1 = lt.SymbolRowTable()
    tab1.add_si_row('I', I, r'\ampere', places=1)
    tab1.add_si_row('B', B[i] * 1e3, r'\milli\tesla')

    tab2 = lt.SymbolColumnTable()
    tab2.add_si_column(r'\Del{s}', Δs, places=0)
    tab2.add_si_column(r'\del{s}', δs, places=0)
    tab2.add_si_column(r'\Del{E}', lt.vals(ΔE) * 1e24, r'\joule', exp='e-24', figures=4)

    tab3 = lt.SymbolRowTable()
    tab3.add_si_row(r'\mean{\Del{E}}', ΔE_mean[i] * 1e24, r'\joule', exp='e-24')
    tab3.add_si_row(r'\Del{E}_{\t{theor}}', ΔE_theory(B[i]) * 1e24, r'\joule', exp='e-24')

    tab = lt.Table()
    tab.add(lt.Row())
    tab.add_hrule()
    tab.add(lt.Row())
    tab.add_hrule()
    tab.add(lt.Row())
    tab.add(lt.Column([tab1, tab2, tab3], "@{}c@{}"))
    tab.savetable(lt.new('table/' + name + '.tex'))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(np.arange(len(row_noB)) + x_min, row_noB, 'b-')
    ax.plot(np.arange(len(row_B)) + x_min, row_B, 'g-')
    ax.plot(peaks_noB[0] + x_min, peaks_noB[1], 'cx')
    ax.plot(peaks_B[0] + x_min, peaks_B[1], 'rx')
    ax.plot([], [], 'b-x', mec='c', label='$B = 0$')
    ax.plot([], [], 'g-x', mec='r', label=r'$B \ne 0$')
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'Graustufenwert in Zeile \num{' + str(y) + '}')
    ax.xaxis.set_major_formatter(lt.SiFormatter())
    ax.yaxis.set_major_formatter(lt.SiFormatter())
    ax.legend(loc='lower center')
    fig.savefig(lt.new('graphic/' + name + '.pdf'))

g_red = ΔE_mean[0] / (μ_B * B[0])
g_P = ΔE_mean[2] / (μ_B * B[2])
g_S = ΔE_mean[2] / (μ_B * B[2]) + ΔE_mean[1] / (μ_B * B[1])

tab = lt.SymbolColumnTable()
tab.add_column(lt.Column([lt.Cell([r'$^1 \t{P}_1$', r'$^1 \t{D}_2$'])], 'l'))
tab.add_si_column('L', L[:2])
tab.add_si_column('M', M[:2])
tab.add_si_column('J', J[:2])
tab.add_si_column('S', S[:2])
tab.add_si_column(r'g_{\t{theor}}', g[:2], figures=2)
tab.add_si_column(r'g_{\t{exp}}', [g_red, g_red])
tab.savetable(lt.new('table/g-red.tex'))

tab = lt.SymbolColumnTable()
tab.add_column(lt.Column([lt.Cell([r'$^3 \t{S}_1$', r'$^3 \t{P}_1$'])], 'l'))
tab.add_si_column('L', L[2:])
tab.add_si_column('M', M[2:])
tab.add_si_column('J', J[2:])
tab.add_si_column('S', S[2:])
tab.add_si_column(r'g_{\t{theor}}', g[2:], figures=2)
tab.add_si_column(r'g_{\t{exp}}', [g_S, g_P])
tab.savetable(lt.new('table/g-blue.tex'))
