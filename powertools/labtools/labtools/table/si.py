from decimal import Decimal
from math import floor, log10
import numpy
import uncertainties.unumpy

from labtools.common import round_figures, round_places, round_to, stds, vals
from labtools.table.common import table_format
from labtools.table.column import Column
from labtools.table.row import Row
from labtools.table.table import Table

class SiColumn(Column):
    def __init__(self, cells, header=None, footer=None):
        Column.__init__(self, [], "", header, footer, columns=1)
        for c in cells:
            self.add_cell(c)

    def add_cell(self, cell):
        Column.add_cell(self, cell)
        self._alignment = "S[table-format={:s}]".format(table_format([d for c in self._cells for d in c.data()]))

class SymbolRowTable(Table):
    def __init__(self, right=False):
        self._right = right
        if right:
            Table.__init__(self, "c", "c")
        else:
            Table.__init__(self, "c c")
        self._columns.append(SiColumn([]))

    def add_si_row(self, symbol, data, unit="", figures=None, places=None, exp="", error=None):
        if figures == None and places == None:
            figures = 1
        if numpy.shape(data) != () and numpy.shape(data) != (1,):
            if figures == None or isinstance(figures, int):
                figures = [figures] * len(data)
            if places == None or isinstance(places, int):
                places = [places] * len(data)
            rounded = []
            for d, f, p in zip(data, figures, places):
                if f != None:
                    rounded.append(round_figures(str(d), f))
                else:
                    rounded.append(round_places(str(d), p))
            data = rounded
        else:
            if isinstance(data, (list, tuple)):
                data = data[0]
            elif isinstance(data, numpy.ndarray):
                data = numpy.asscalar(data)
            if error == None and not stds(data) == 0:
                error = float(stds(data))
                data = float(vals(data))
            else:
                error = None
            if error != None:
                if figures != None:
                    error = round_figures(str(error), figures)
                    data = round_to(str(data), error, figures=figures)
                else:
                    error = round_places(str(error), places)
                    data = round_places(str(data), places)
            else:
                if figures != None:
                    data = round_figures(str(data), figures)
                else:
                    data = round_places(str(data), places)
        if self._right:
            self.add_row(Row("$" + (symbol if error == None else r"\errPhantom{{{:s}}}".format(symbol)) + "$", r"\si{{{:s}}}".format(unit) if exp == "" else r"\SI{{{:s}}}{{{:s}}}".format(exp, unit)))
        else:
            self.add_row(Row("$" + (symbol if error == None else r"\errPhantom{{{:s}}}".format(symbol)) + "$ & " + (r"\si{{{:s}}}".format(unit) if exp == "" else r"\SI{{{:s}}}{{{:s}}}".format(exp, unit))))
        self._columns[0].add_cell(data)
        if error != None:
            if self._right:
                self.add_row(Row(r"$\err{{{:s}}}$".format(symbol), r"\si{{{:s}}}".format(unit) if exp == "" else r"\SI{{{:s}}}{{{:s}}}".format(exp, unit)))
            else:
                self.add_row(Row(r"$\err{{{:s}}}$".format(symbol) + " & " + (r"\si{{{:s}}}".format(unit) if exp == "" else r"\SI{{{:s}}}{{{:s}}}".format(exp, unit))))
            self._columns[0].add_cell(error)

class SymbolColumnTable(Table):
    def __init__(self, **kwargs):
        Table.__init__(self, **kwargs)
        self._rows.append(Row())

    def add_si_column(self, symbol, data, unit="", figures=None, places=None, exp="", squeeze_unit=False, errors=None, zero_error_figures=None, zero_error_places=None):
        if errors == None and any(stds(data)):
            errors = stds(data)
        data = vals(data)
        if errors == None:
            errors = [None] * len(data)
        if figures == None and places == None:
            figures = 1
        if figures == None or isinstance(figures, int):
            figures = [figures] * len(data)
        if places == None or isinstance(places, int):
            places = [places] * len(data)
        if zero_error_figures == None and zero_error_places == None:
            zero_error_figures = 1
        if zero_error_figures == None or isinstance(zero_error_figures, int):
            zero_error_figures = [zero_error_figures] * len(data)
        if zero_error_places == None or isinstance(zero_error_places, int):
            zero_error_places = [zero_error_places] * len(data)
        cell = []
        if any(errors):
            ecell = []
            for d, f, p, e, zef, zep in zip(data, figures, places, errors, zero_error_figures, zero_error_places):
                if not numpy.isfinite(d):
                    cell.append('')
                    ecell.append('')
                elif not numpy.isfinite(e):
                    if zef != None:
                        d = round_figures(str(d), zef)
                    else:
                        d = round_places(str(d), zep)
                    cell.append(d)
                    ecell.append('')
                elif e == 0:
                    if zef != None:
                        d = round_figures(str(d), zef)
                    else:
                        d = round_places(str(d), zep)
                    cell.append(d)
                    ecell.append('0')
                else:
                    if f != None:
                        e = round_figures(str(e), f)
                        d = round_to(str(d), e, figures=f)
                    else:
                        e = round_places(str(e), p)
                        d = round_places(str(d), p)
                    cell.append(d)
                    ecell.append(e)
        else:
            for d, f, p in zip(data, figures, places):
                if not numpy.isfinite(d):
                    cell.append('')
                else:
                    if f != None:
                        d = round_figures(str(d), f)
                    else:
                        d = round_places(str(d), p)
                    cell.append(d)
        if symbol is not None:
            header = build_header(symbol, unit, exp, squeeze_unit)
        else:
            header = None
        self.add(SiColumn([cell], header))
        if any(errors):
            eheader = build_header(r"\err{{{:s}}}".format(symbol), unit, exp, squeeze_unit)
            self.add(SiColumn([ecell], eheader))

def build_header(symbol, unit, exp, squeeze_unit):
    header = r"\multicolumn{{1}}{{c}}{{${:s}$}}".format(symbol)
    if unit != "" or exp != "":
        if unit != "":
            if exp != "":
                h2 = r"\SI{{{:s}}}{{{:s}}}".format(exp, unit)
            else:
                h2 = r"\si{{{:s}}}".format(unit)
        else:
            h2 = r"\num{{{:s}}}".format(exp)
        if squeeze_unit:
            header = [header, r"\multicolumn{{1}}{{c}}{{$/\:{:s}\hphantom{{\:/}}$}}".format(h2)]
        else:
            header = r"\multicolumn{{1}}{{c}}{{${:s}\:/\:{:s}$}}".format(symbol, h2)
    return header

# deprecated, use SymbolColumnTable.add_si_column
class SymbolColumn(SiColumn):
    def __init__(self, symbol, data, unit="", figures=None, places=None, exp="", squeeze_unit=False):
        header = build_header(symbol, unit, exp, squeeze_unit)
        if figures == None or isinstance(figures, int):
            figures = [figures] * len(data)
        if places == None or isinstance(places, int):
            places = [places] * len(data)
        cell = []
        for d, f, p in zip(data, figures, places):
            if not numpy.isfinite(uncertainties.unumpy.nominal_values(d)):
                cell.append('')
            else:
                if f != None:
                    cell.append(round_figures(str(uncertainties.unumpy.nominal_values(d)), f))
                elif p != None:
                    cell.append(round_places(str(uncertainties.unumpy.nominal_values(d)), p))
                else:
                    cell.append(round_places(str(uncertainties.unumpy.nominal_values(d)), 6))
        SiColumn.__init__(self, [cell], header)
