import os
import os.path

from labtools.table.row import Row, HorizontalRule
from labtools.table.column import Column, VerticalRule

class Table:
    def __init__(self, leftAlignment="", rightAlignment="", leftColumns=None, rightColumns=None, headerRow=None, footerRow=None, valign=None):
        self._leftAlignment = leftAlignment
        self._rightAlignment = rightAlignment
        if leftColumns == None:
            if leftAlignment == "":
                leftColumns = 0
            else:
                leftColumns = len(leftAlignment.split(" "))
        self._leftColumns = leftColumns
        if rightColumns == None:
            if rightAlignment == "":
                rightColumns = 0
            else:
                rightColumns = len(rightAlignment.split(" "))
        self._rightColumns = rightColumns
        if headerRow == None:
            headerRow = Row()
        self._headerRow = headerRow
        if footerRow == None:
            footerRow = Row()
        self._footerRow = footerRow
        self._valign = valign
        self._rows = []
        self._columns = []

    def add_row(self, row):
        self._rows.append(row)
        row.set_left_columns(self._leftColumns)
        row.set_right_columns(self._rightColumns)

    def add_column(self, column):
        self._columns.append(column)

    def add_vrule(self, rules=1):
        self.add(VerticalRule(rules))

    def add_hrule(self, rules=1):
        self.add(HorizontalRule(rules))

    def add(self, thing):
        if isinstance(thing, Row):
            self.add_row(thing)
        elif isinstance(thing, Column):
            self.add_column(thing)

    def render(self, inside=False):
        text = r"\begin{tabular}"
        if self._valign is not None:
            text += "[" + self._valign + "]"
        text += "{"
        if self._leftAlignment != "":
            text += self._leftAlignment + " "
        text += " ".join([ c.alignment() for c in self._columns ])
        if self._rightAlignment != "":
            text += " " + self._rightAlignment
        text += "}\n"
        if not inside:
            text += r"  \toprule" + "\n"
        text += self._headerRow.render([[ c.header() for c in self._columns ]], self._leftColumns, self._rightColumns)
        if self._headerRow.rows() > 0 or any([ c.header().rows() for c in self._columns ]):
            text += r"  \midrule" + "\n"
        j = 0
        for i in range(len(self._rows)):
            text += self._rows[i].render([ [ c.cell(k) for c in self._columns ] for k in range(j, j + self._rows[i].cell_rows()) ], self._leftColumns, self._rightColumns)
            j += self._rows[i].cell_rows()
        if self._footerRow.rows() > 0 or any([ c.footer().rows() for c in self._columns ]):
            text += r"  \midrule" + "\n"
        text += self._footerRow.render([[ c.footer() for c in self._columns ]], self._leftColumns, self._rightColumns)
        if not inside:
            text += r"  \bottomrule" + "\n"
        text += r"\end{tabular}"
        return text

    def __str__(self):
        return self.render(inside=True)

    def savetable(self, path):
        if os.path.dirname(path) != '' and not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w') as f:
            f.write(self.render())
