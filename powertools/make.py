import sys
import os
import os.path
import re
import networkx as nx
import subprocess
from labtools.common import get_new_file_path, get_file_path

ignore_dirs = [r"^\.git$",
               r"^__pycache__$",
               r"^build$",
               r"^labtools$",
               r"^test$"]

def handle_tex(path):
    s = open(get_file_path(path), "r").read()
    m = re.findall(r'\\input{(.+?)}', s)
    m += [v[1] for v in re.findall(r'\\includes?graphics(\[.*?\])?{(.+?)}', s)]
    m += [v[1] for v in re.findall(r'\\addbibresource(\[.*?\])?{(.+?)}', s)]
    for d in m:
        handle_file(d)
        g.add_edge(d, path)
    if len(re.findall(r'\\begin{document}', s)) > 0:
        handle_file(path[:-3] + 'pdf')
        g.add_edge(path, path[:-3] + 'pdf')
        g.node[path[:-3] + 'pdf']['make'] = create_make(make_tex, path)

def handle_py(path):
    s = open(get_file_path(path), "r").read()
    m = re.findall(r'''lt.file\(r?["'](.+?)["']\)''', s)
    for d in m:
        handle_file(d)
        g.add_edge(d, path)
    m = re.findall(r'''savefig\(.*?lt.new\(r?["'](.+?\.tex)["']\).*?\)''', s)
    for d in m:
        handle_file('matplotlibrc')
        g.add_edge('matplotlibrc', path)
        handle_file('header/matplotlib.tex')
        g.add_edge('header/matplotlib.tex', path)
        handle_file(d)
        handle_file(d[:-3] + 'pdf')
        g.add_edge(d, d[:-3] + 'pdf')
        g.node[d[:-3] + 'pdf']['make'] = create_make(make_tex_crop, d)
    m = re.findall(r'''lt.new\(r?["'](.+?)["']\)''', s)
    for d in m:
        handle_file(d)
        g.add_edge(path, d)
        g.node[d]['make'] = create_make(make_py, path)

def handle_crop(path):
    s = open(get_file_path(path), "r").read()
    s = s.splitlines()
    handle_file(s[0])
    handle_file(path[:-4] + 'pdf')
    g.add_edge(s[0], path)
    g.add_edge(path, path[:-4] + 'pdf')
    g.node[path[:-4] + 'pdf']['make'] = create_make_crop(s[0], s[1], s[2], path[:-4] + 'pdf')

def make_py(path):
    print(r"TEXINPUTS=`pwd`/./build/:`pwd`/./:`pwd`/../:`pwd`/../powertools/header/:`pwd`/../powertools/header/texmf//: PYTHONPATH=./build/script/:./script/:../script/:../powertools/labtools/ MATPLOTLIBRC=`pwd`/../powertools/ python3 " + path)
    p = subprocess.call(r"TEXINPUTS=`pwd`/./build/:`pwd`/./:`pwd`/../:`pwd`/../powertools/header/:`pwd`/../powertools/header/texmf//: PYTHONPATH=./build/script/:./script/:../script/:../powertools/labtools/ MATPLOTLIBRC=`pwd`/../powertools/ python3 " + path, shell=True)
    print()

def make_tex(path):
    print(r"""TEXINPUTS=./build/:./:../:../powertools/header/:../powertools/header/texmf//: max_print_line=1048576 lualatex --shell-escape --interaction=batchmode --output-directory={0} {1}.tex""".format("./build/" + os.path.dirname(path), os.path.basename(path)[:-4]))
    p = subprocess.Popen(r"""TEXINPUTS=./build/:./:../:../powertools/header/:../powertools/header/texmf//: max_print_line=1048576 lualatex --shell-escape --interaction=batchmode --output-directory={0} {1}.tex > /dev/null ;
                            cd {0} ;
                            rubber-info --errors {1}.log ;
                            rubber-info --warnings {1}.log ;
                            rubber-info --refs {1}.log ;
                            rubber-info --boxes {1}.log""".format("./build/" + os.path.dirname(path), os.path.basename(path)[:-4]), shell=True, stdout=subprocess.PIPE)
    o = p.communicate()[0].decode('UTF-8')
    m = re.findall(r"\[rerunfilecheck\]", o)
    m += re.findall(r"Label\(s\)", o)
    n = re.findall(r"\[biblatex\]", o)
    if len(n) > 0:
        print(r"""BIBINPUTS=./build/:./:../:../powertools/: biber --logfile {} --outfile {} {}""".format(get_new_file_path(path[:-3] + 'blg'), get_new_file_path(path[:-3] + 'bbl'), get_new_file_path(path[:-3] + 'bcf')))
        p = subprocess.Popen(r"""BIBINPUTS=./build/:./:../:../powertools/: biber --logfile {} --outfile {} {}""".format(get_new_file_path(path[:-3] + 'blg'), get_new_file_path(path[:-3] + 'bbl'), get_new_file_path(path[:-3] + 'bcf')), shell=True, stdout=subprocess.PIPE)
        p.communicate()
    if len(m) > 0 or len(n) > 0:
        make_tex(path)
    else:
        print(o)

def make_tex_crop(path):
    make_tex(path)
    print(r"../powertools/pdfcrop2.pl {} {}".format(get_file_path(path[:-3] + 'pdf'), get_new_file_path(path[:-3] + 'pdf')))
    p = subprocess.Popen(r"../powertools/pdfcrop2.pl {} {}".format(get_file_path(path[:-3] + 'pdf'), get_new_file_path(path[:-3] + 'pdf')), shell=True, stdout=subprocess.PIPE)
    p.communicate()
    print()

def make_crop(source, page, box, out):
    print(r"pdfseparate -f {0} -l {0} {1} {2}".format(page, get_file_path(source), get_new_file_path(out)))
    p = subprocess.Popen(r"pdfseparate -f {0} -l {0} {1} {2}".format(page, get_file_path(source), get_new_file_path(out)), shell=True, stdout=subprocess.PIPE)
    p.communicate()
    print(r"../powertools/pdfcrop2.pl --mode absolute --margins '{}' {} {}".format(box, get_new_file_path(out), get_new_file_path(out)))
    p = subprocess.Popen(r"../powertools/pdfcrop2.pl --mode absolute --margins '{}' {} {}".format(box, get_new_file_path(out), get_new_file_path(out)), shell=True, stdout=subprocess.PIPE)
    p.communicate()
    print(r"../powertools/pdfcrop2.pl {} {}".format(get_new_file_path(out), get_new_file_path(out)))
    p = subprocess.Popen(r"../powertools/pdfcrop2.pl {} {}".format(get_new_file_path(out), get_new_file_path(out)), shell=True, stdout=subprocess.PIPE)
    p.communicate()
    print()

def create_make(func, path):
    def a(tmp=None):
        func(path)
    return a

def create_make_crop(source, page, box, out):
    def a(tmp=None):
        make_crop(source, page, box, out)
    return a

def handle_file(path):
    if not g.has_node(path):
        g.add_node(path)
        g.node[path]['valid'] = False
        g.node[path]['visited'] = False
        g.node[path]['make'] = None
        if os.path.exists(get_file_path(path)):
            if path.endswith('.tex'):
                handle_tex(path)
            elif path.endswith('.py'):
                handle_py(path)
            elif path.endswith('.crop'):
                handle_crop(path)

g = nx.DiGraph()

for d in ["./", "../powertools/"]:
    for root, dirs, files in os.walk(d):
        for c in dirs:
            for p in ignore_dirs:
                if re.search(p, c) != None:
                    dirs.remove(c)
        for f in files:
            if root[len(d):] == '':
                handle_file(f)
            else:
                handle_file(root[len(d):] + "/" + f)

if len(sys.argv) > 1:
    target = sys.argv[1]
else:
    target = 'main.pdf'

h = g.reverse()
l = [ n for n in nx.dfs_preorder_nodes(h, target) ]
G = g.subgraph(l)

os.makedirs("./build", exist_ok=True)

while not G.node[target]['visited']:
    for n in nx.topological_sort(G):
        if G.node[n]['visited']:
            continue
        if os.path.isfile(get_file_path(n)):
            t = os.stat(get_file_path(n)).st_mtime
            G.node[n]['time'] = t
            for p in G.predecessors_iter(n):
                if G.node[p]['time'] > t:
                    G.node[n]['time'] = G.node[p]['time']
                    break
            if G.node[n]['time'] > t:
                if G.node[n]['make'] != None:
                    G.node[n]['make'](n)
                    G.node[n]['time'] = os.stat(get_file_path(n)).st_mtime
        else:
            if G.node[n]['make'] != None:
                G.node[n]['make'](n)
            G.node[n]['time'] = os.stat(get_file_path(n)).st_mtime
        G.node[n]['visited'] = True
        break
