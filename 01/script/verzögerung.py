from labtools import *

T_VZ, N = np.loadtxt(lt.file('data/verzögerung'), unpack=True)

tab = lt.SymbolColumnTable()
tab.add_si_column(r'T_{\t{VZ}}', T_VZ, r'\nano\second', places=1)
tab.add_si_column('N', N, r'\per\second', places=1)
tab.savetable(lt.new('table/verzögerung.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(T_VZ, N, 'rx', label='Messwerte')
ax.set_xlim(6.9, 9.1)
ax.set_xlabel(r'$\silabel{T_{\t{VZ}}}{\nano\second}$')
ax.set_ylabel(r'\silabel{N}{\per\second}')
ax.xaxis.set_major_formatter(lt.SiFormatter(1))
ax.yaxis.set_major_formatter(lt.SiFormatter(1))
ax.legend()
fig.savefig(lt.new('graphic/verzögerung.pdf'))
