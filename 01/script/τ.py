from labtools import *

N_start = np.loadtxt(lt.file('data/imp_start'), unpack=True)
N_stop = np.loadtxt(lt.file('data/imp_stop'), unpack=True)
T = np.loadtxt(lt.file('data/T'), unpack=True)
T_s = np.loadtxt(lt.file('data/univibrator')) * 1e-6
τ_lit = np.loadtxt(lt.file('data/τ_lit'))
τ_lit = unc.ufloat(τ_lit[0], τ_lit[1])

b, N = np.loadtxt(lt.file('data/hist'), unpack=True)
b = b[3:436]
N = N[3:436]
N = unc.unumpy.uarray(N, np.sqrt(N))

A, B = np.loadtxt(lt.file('result/calibration'))
A = A[0]
B = B[0]

t = (b - B) / A

def exponential(t, λ, N0, U):
    return N0 * np.exp(- λ * t) + U

λ, N0, U = lt.ucurve_fit(exponential, t, N)
τ = 1 / λ

U_poisson = (1 - np.exp(-N_start / T * T_s)) * N_start / len(b)

x = np.linspace(-1e-6, 51e-6, 10000)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.errorbar(t, lt.vals(N), yerr=lt.stds(N), fmt='rx', label='Messwerte')
ax.plot(x, exponential(x,lt.vals(λ), lt.vals(N0), lt.vals(U)), 'b-', label='Ausgleichskurve')
ax.set_xlim(-1e-6, 51e-6)
ax.set_ylim(-20, 600)
ax.set_xlabel(r'\silabel{t}{\micro\second}')
ax.set_ylabel(r'$N$')
ax.xaxis.set_major_formatter(lt.SiFormatter(factor=1e6))
ax.yaxis.set_major_formatter(lt.SiFormatter())
lt.ax_reverse_legend(ax, 'upper right')
fig.savefig(lt.new('graphic/τ.pdf'))

tab = lt.SymbolRowTable()
tab.add_si_row(r'N_{\t{start}}', N_start, places=0)
tab.add_si_row(r'N_{\t{stop}}', N_stop, places=0)
tab.add_si_row('T', T, r'\second', places=0)
tab.add_si_row(r'T_{\t{s}}', T_s * 1e6, r'\micro\second', places=0)
tab.add_hrule()
tab.add_si_row('λ', λ * 1e-6, r'\per\micro\second')
tab.add_si_row('N_0', N0)
tab.add_si_row('U', U)
tab.add_hrule()
tab.add_si_row(r'U_{\t{Poisson}}', U_poisson, places=1)
tab.add_hrule()
tab.add_si_row('τ', τ * 1e6, r'\micro\second')
tab.add_si_row(r'τ_{\t{lit}}', τ_lit, r'\micro\second')
tab.savetable(lt.new('table/τ.tex'))
