from labtools import *

t, b = np.loadtxt(lt.file('data/calibration'), unpack=True)
t *= 0.1e-6

A, B = lt.linregress(t, b)

np.savetxt(lt.new('result/calibration'), [[lt.vals(A),lt.stds(A)], [lt.vals(B), lt.stds(B)]])

tab = lt.SymbolColumnTable()
tab.add_si_column('t', t * 1e6, r'\micro\second', places=1)
tab.add_si_column('b', b, r'', places=0)

tab2 = lt.SymbolRowTable()
tab2.add_si_row('A', A * 1e-6, r'\per\micro\second')
tab2.add_si_row('B', B, r'')

tab3 = lt.Table()
tab3.add(lt.Row())
tab3.add_hrule()
tab3.add(lt.Row())
tab3.add(lt.Column([tab, tab2], "@{}c@{}"))
tab3.savetable(lt.new('table/calibration.tex'))

x = np.linspace(0, 10e-6, 10000)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, lt.vals(A * x + B), 'b-', label='Ausgleichsgerade')
ax.plot(t, b, 'rx', label='Messwerte')
ax.set_xlabel(r'\silabel{t}{\micro\second}')
ax.set_ylabel(r'$b$')
ax.xaxis.set_major_formatter(lt.SiFormatter(factor=1e6))
ax.yaxis.set_major_formatter(lt.SiFormatter())
lt.ax_reverse_legend(ax, 'upper left')
fig.savefig(lt.new('graphic/calibration.pdf'))
