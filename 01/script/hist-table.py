from labtools import *

b, N = np.loadtxt(lt.file('data/hist'), unpack=True)

tab = lt.SymbolColumnTable()
for i in range(8):
    tab.add_si_column('b', b[i * len(b) / 16:(i + 1) * len(b) / 16], places=0)
    tab.add_si_column('N', N[i * len(b) / 16:(i + 1) * len(b) / 16], places=0)
    if i != 7:
        tab.add(lt.VerticalSpace(len(b) / 16, '1em'))
tab.savetable(lt.new('table/hist1.tex'))

tab = lt.SymbolColumnTable()
for i in range(8, 16):
    tab.add_si_column('b', b[i * len(b) / 16:(i + 1) * len(b) / 16], places=0)
    tab.add_si_column('N', N[i * len(b) / 16:(i + 1) * len(b) / 16], places=0)
    if i != 15:
        tab.add(lt.VerticalSpace(len(b) / 16, '1em'))
tab.savetable(lt.new('table/hist2.tex'))
