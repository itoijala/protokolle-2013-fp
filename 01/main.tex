\input{header/report.tex}

\addbibresource{lit.bib}

\SetExperimentNumber{01}

\begin{document}
  \maketitlepage{Lebensdauer kosmischer Myonen}{20.02.2013}{18.04.2013}
  \section{Ziel}
    Ziel des Versuchs ist es, die Lebensdauer $τ$ des Myons zu bestimmen.
  \section{Theorie}
    \subsection{Myonen}
      Myonen sind geladene Leptonen der zweiten Familie.
      Sie entstehen beispielsweise in der oberen Atmosphäre durch Zerfälle von Pionen, die aus Reaktionen der kosmischen Strahlung mit der Atmosphäre stammen.
      In den Prozessen
      \begin{eqns}
        \HepProcess{\Ppiminus &\to& \Pmuon \, \APnum} \\
        \HepProcess{\Ppiplus &\to& \APmuon \, \Pnum} ,
      \end{eqns}
      die über die schwache Wechselwirkung vermittelt werden, entstehen außerdem Neutrinos, die aber bei diesem Versuch nicht von Bedeutung sind.
      Die Zeitdilatation aufgrund ihrer hohen Gechwindigkeit ermöglicht es den Myonen, den Erdboden trotz ihrer Lebenszeit von $τ \approx \SI{1}{\micro\second}$ zu erreichen.
      Myonen zerfallen schwach in Elektronen und Neutrinos.
      Dies geschieht durch die Prozesse
      \begin{eqns}
        \HepProcess{\Pmuon &\to& \Pelectron \, \APnue \, \Pnum} \\
        \HepProcess{\APmuon &\to& \Ppositron \, \Pnue \, \APnum} .
      \end{eqns}
    \subsection{Lebensdauer} \label{lebensdauer}
      Die Wahrscheinlichkeit, dass ein Myon während des Zeitintervalls $\intcc{t}{t + \dif{t}}$ zerfällt, beträgt
      \begin{eqn}
        \dif{w} = λ \dif{t} ,
      \end{eqn}
      wobei $λ$ eine Konstante ist, die für alle Myonen gleich ist.

      Für eine Probe von $N$ Myonen gilt für die Änderung der Anzahl während einer Zeit $\dif{t}$
      \begin{eqn}
        \dif{N} = - λ N \dif{t} .
      \end{eqn}
      Für große $N$ folgt
      \begin{eqn}
        \frac{\f{N}(t)}{N_0} = \E^{- λ t} ,
      \end{eqn}
      wobei $N_0 = \f{N}(0)$ die Anzahl der Myonen am Anfang ist.
      Für die Anzahl der Myonen, deren Lebensdauer im Intervall $\intcc{t}{t + \dif{t}}$ liegt, gilt
      \begin{eqn}
        \frac{\dif{\f{N}(t)}}{N_0} = \frac{\f{N}(t) - \f{N}(t + \dif{t})}{N_0} = λ \E^{- λ t} \dif{t} .
      \end{eqn}
      Diese Verteilungsfunktion für die Lebensdauer wird als Exponentialverteilung bezeichnet.

      Für den Erwartungswert einer Verteilung $f$ gilt
      \begin{eqn}
        \avg{x} = \int^{} x \f{f}(x) \dif{x} .
      \end{eqn}
      Mit dieser Definition gilt für die Lebensdauer
      \begin{eqn}
        τ = \avg{t} = \int_0^\infty t λ \E^{- λ t} \dif{t} = \frac{1}{λ} . \eqlabel{eqn:lebensdauer}
      \end{eqn}
      Die Lebensdauer kann durch die Messung der Zerfallskonstante $λ$ bestimmt werden.
    \subsection{Nachweis von Myonen} \label{nachweis}
      Myonen können mit einem Szintillator nachgewiesen werden.
      Dabei regt ein Myon Moleküle des Szintillatormaterials durch Abgabe von Teilen seiner kinetischen Energie an.
      Die Moleküle wechseln zurück in ihren Grundzustand und emittieren dabei Licht, welches durch Sekundärelektronenvervielfacher detektiert werden kann.
      Wird ein Myon durch diesen Prozess zur Ruhe abgebremst, zerfällt es im Szintillatorvolumen.
      Dabei entstehen hochenergetische Elektronen oder Positronen, die wiederum, genauso wie Myonen, mit dem Material wechselwirken und detektiert werden können.
      Der Eintritt und der Zerfall eines Myons werden beide durch Lichtsignale aus dem Szintillatorvolumen gekennzeichnet.
  \section{Aufbau}
    Eine graphische Darstellung des Aufbaus befindet sich in Abbildung \ref{fig:aufbau}.
    Wie in Abschnitt \ref{nachweis} erklärt, wird zum Nachweis der Myonen ein Szintillator verwendet.
    Der Szintillator hat einen Volumen von $V \approx \SI{50}{\litre}$ und befindet sich in einem Edelstahlzylinder mit konischen Enden.
    Der Szintillator besteht aus dem organischen Material NE-102 und ist in Toluol gelöst, um eine hinreichend kleine Abklingzeit von etwa $\SI{10}{\nano\second} \ll τ$ zu erreichen.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/aufbau.pdf}
      \caption{Aufbau \cite{anleitung01}}
      \label{fig:aufbau}
    \end{figure}

    An beiden Enden des Szintillators ist jeweils ein Sekundärelektronenvervielfacher angebracht, der die Lichtsignale in elektrische Signale übersetzt.
    An den Sekundärelektronenvervielfachern ist jeweils ein Diskriminator angeschlossen, der schwache Signale aussortiert, um Rauschen zu vermeiden.
    Die Schwellen der Diskriminatoren und die Längen der resultierenden Signale sind anpassbar.
    Zwischen einem der Sekundärelektronenvervielfachern und seinem Diskriminator wird eine einstellbare Verzögerung geschaltet, um Unterschiede zwischen den Sekundärelektronenvervielfachern auszugleichen.

    Die Diskriminatoren sind an einem Koinzidenzfilter angeschlossen, der ein Signal herausfiltert, falls es nicht innerhalb von \SI{4}{\nano\second} von beiden Diskriminatoren geliefert wird.
    Dies dient dazu, Rauschsignale der Sekundärelektronenvervielfacher herauszufiltern, da Rauschsignale zwischen beiden Detektoren unkorreliert sind.
    Am Koinzidenzfilter ist auch ein Doppelpulsgenerator mit einstellbarem Pulsabstand angeschlossen, der dazu dient, das System zu kalibrieren.

    Zur Messung der Individuallebensdauern der einzelnen Myonen wird ein Zeit-Amplituden-Converter verwendet.
    Dieser verwandelt die Zeit zwischen dem Start- und dem Stoppimpuls in die Amplituden seines Ausgangssignals, welches von einem Vielkanalanalysator aufgenommen wird.
    Ein Univibrator wird zum Herausfiltern von Ereignissen verwendet, bei denen das Myon nicht innerhalb des Szintillators zerfällt.
    Die Steuerung der Uhr wird durch zwei Undgatter realisiert.
    Beim ersten Signal wird die Uhr gestartet und der Univibrator geschaltet.
    Dieser wartet eine einstellbare Zeit auf das zweite Signal.
    Kommt innerhalb der Wartezeit kein Signal, schaltet der Univibrator wieder zurück in den Anfangszustand und das nächste Signal startet die Uhr erneut.
    Wird während der Wartezeit ein zweites Signal detektiert, wird die Uhr gestoppt und das Signal an den Vielkanalanalysator weitergeleitet.
    Dabei geht die Schaltung wieder in den Anfangszustand über.
    Die Verzögerung vor dem Univibrator dient dazu, eine eindeutige Reihenfolge zwischen dem Einschalten der Uhr und dem Umschalten des Univibrators zu gewährleisten.
    Ein Impulszähler kann an die Ausgänge der Undgatter angeschlossen werden, um die Start- und Stoppereignisse zu zählen.
  \section{Durchführung}
    Die Schwelle der Diskriminatoren wird so eingestellt, dass \numrange{20}{40} Impulse pro Sekunde gemessen werden.
    Hierzu wird der Impulszähler an den Diskriminatorausgang geschaltet.
    Im Experiment wurde an beiden Diskriminatoren eine Rate von $\SI{29}{\per\second}$ eingestellt.
    Die Verzögerung eines der Sekundärelektronenvervielfacher wird so eingestellt, dass nach dem Koinzidenzfilter die Rate maximal wird.
    Im Experiment wurde eine Verzögerung von $\SI{8}{\nano\second}$ verwendet.

    Der maximale Abstand von Start- und Stopppulsen der Uhr sowie die Suchzeit des Univibrators werden auf $T_{\t{s}} = \SI{50}{\micro\second}$ eingestellt.
    Die Wartezeit des Univibrators wird auf wenige Mikrosekunden unter diesen Wert eingestellt um eine möglichst gute Auflösung zu erreichen, ohne Teile des Signals zu verlieren.
    Der Doppelimpulsgenerator wird statt den Diskriminatoren an den Koinzidenzfilter angeschlossen.
    Es wird der Abstand der Impulse erhöht und am Vielkanalanalysator abgelesen, bei welchem Abstand welcher Kanal Signale registriert.
    Dieser lineare Zusammenhang kann dazu verwendet werden, von der Kanalnummer auf den zeitlichen Abstand der Signale zu schließen.

    Der Doppelimpulsgenerator wird entfernt und die Diskriminatoren angeschlossen.
    Der Impulszähler wird verwendet, um die Anzahle $N_{\t{start}}$ und $N_{\t{stop}}$ der Start- und Stoppsignale zu messen.
    Die Messzeit $T$ wird von einer Uhr gemessen.
    Die Messung wird für eine Messdauer von \SIrange{20}{30}{\hour} laufen gelassen.
  \section{Messwerte und Auswertung}
    \subsection{Ermittlung der optimalen Verzögerungszeit}
      Die Messwerte zur Ermittlung der optimalen Verzögerungszeit stehen in Tabelle \ref{tab:verzögerung} und sind in Abbildung \ref{fig:verzögerung} aufgetragen.
      Für die Zeit können diskrete Werte im Abstand von \SI{0.5}{\nano\second} eingestellt werden.
      Für die weitere Messung wird der Wert $T_{\t{VZ}} = \SI{8.0}{\nano\second}$ gewählt, da dieser die höchste Rate liefert.
      \begin{table}
        \caption{Messwerte zur Ermittlung der optimalen Verzögerungszeit}
        \label{tab:verzögerung}
        \input{table/verzögerung.tex}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/verzögerung.pdf}
        \caption{Messwerte zur Ermittlung der optimalen Verzögerungszeit}
        \label{fig:verzögerung}
      \end{figure}
    \subsection{Kalibrierung des Vielkanalanalysators}
      Die Lebensdauer der Myonen unterliegt laut Abschnitt \ref{lebensdauer} einer Exponentialverteilung.
      Um einen Zusammenhang zwischen dem Doppelimpulsabstand $t$ und dem Index $b$ des Kanals, in dem Signale registriert werden, herzustellen, wird eine Kalibrierung vorgenommen.
      Die ermittelten Messwerte stehen in Tabelle \ref{tab:calibration}.
      Anhand der Messwerte wird eine lineare Regression mit der Funktion
      \begin{eqn}
        b = A t + B
      \end{eqn}
      durchgeführt.
      Die Ergebnisse der Regressionstehen stehen ebenfalls in Tabelle \ref{tab:calibration}.
      Zusätzlich sind die Messwerte und die ermittelte Eichgerade in Abbildung \ref{fig:calibration} aufgezeichnet.
      Daraus folgt für den Doppelimpulsabstand im Folgenden
      \begin{eqn}
        t = \frac{b - B}{A} . \eqlabel{eqn:doppelimpulsabstand}
      \end{eqn}
      \begin{table}
        \caption{Messwerte und Ergebnisse der linearen Regression für die Kalibrierung des Vielkanalanalysators}
        \label{tab:calibration}
        \input{table/calibration.tex}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/calibration.pdf}
        \caption{Messwerte und Eichgerade}
        \label{fig:calibration}
      \end{figure}
    \subsection{Ermittlung der Lebensdauer}
      Die Ergebnisse des Vielkanalanalysators sind für die einzelnen Bins in den Tabellen \ref{tab:hist1} und \ref{tab:hist2} eingetragen.
      \begin{table}
        \caption{Messwerte des Vielkanalanalysators - Teil 1}
        \label{tab:hist1}
        \OverfullCenter{\input{table/hist1.tex}}
      \end{table}
      \begin{table}
        \caption{Messwerte des Vielkanalanalysators - Teil 2}
        \label{tab:hist2}
        \OverfullCenter{\input{table/hist2.tex}}
      \end{table}
      Die Messzeit steht in Tabelle \ref{tab:τ}, ebenso stehen dort die Anzahle der registrierten Start- und Stoppsignale.

      Mit \eqref{eqn:doppelimpulsabstand} werden die Bins aus den Tabellen \ref{tab:hist1} und \ref{tab:hist2} in Doppelimpulsabstände umgerechnet.
      Anschließend wird eine Ausgleichsrechnung mit der Funktion
      \begin{eqn}
        \f{N}(t) = N_0 \E^{-λ t} + U
      \end{eqn}
      durchgeführt, wobei $U$ der Untergrund ist.
      Dabei ist zu beachten, dass $N$ einer Poissonverteilung unterliegt und ein gewichtete Ausgleichsrechnung mit dem Fehler $\err{N} = \sqrt{N}$ für die jeweiligen Bins verwendet wird.
      Es wird die Ausgleichsroutine \texttt{scipy.optimize.curve\_fit} aus \cite{scipy} verwendet.
      Dabei werden die Bins mit dem Wert $0$ aussortiert, weil sie auch den Fehler $0$ haben und somit die Ausgleichsrechnung stören; hier sind es die Bins 0, 1 und alle ab 436.
      Bin 2 wird auch nicht verwendet, da sein Wert eine sehr große Abweichung aufweist, die die Ausgleichsrechnung stört.
      Die Ergebnisse der numerischen Berechnung stehen in Tabelle \ref{tab:τ}
      Des Weiteren sind die Messwerte und die Ausgleichskurve in Abbildung \ref{fig:τ} aufgetragen.
      Aus Formel \eqref{eqn:lebensdauer} ergibt sich die Lebensdauer $τ$, die ebenfalls in Tabelle \ref{tab:τ} aufgeführt ist, des Weiteren steht dort die Suchzeit $T_\t{S}$.
      \begin{table}
        \caption{Start- und Stoppsignale, Messzeit, Suchzeit, Ergebnisse der Ausgleichsrechnung, mittels Poisson abgeschätzter Untergrund, errechnete Lebensdauer der Myonen und Literaturwert $τ_{\t{lit}}$ der Lebensdauer \cite{PhysRevD.86.010001}}
        \label{tab:τ}
        \input{table/τ.tex}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/τ.pdf}
        \caption{Messwerte des Vielkanalanalysators und errechnete Ausgleichsgerade}
        \label{fig:τ}
      \end{figure}

      Der Untergrund kann ebenfalls mittels der Poisson-Verteilung
      \begin{eqn}
        \f{p}(k; λ) = \frac{λ^k}{\factorial{k}} \E^{-λ} ,
      \end{eqn}
      abgeschätzt werden, die die Wahrscheinlichkeit angibt, dass $k$ unabhängige Ereignisse in einem Zeitintervall stattfinden, in dem im Mittel $λ$ Ereignisse gemessen werden.
      Die Wahrscheinlichkeit, dass nach Anfang der Zeitmessung ein weiteres Myon die Apparatur betritt, beträgt
      \begin{eqn}
        P = 1 - \f{p}(0; \frac{N_{\t{start}}}{T} T_{\t{s}}) ,
      \end{eqn}
      wobei der eingesetzte Wert von $λ$ die Anzahl der zu erwartenden Myonen innerhalb der Suchzeit ist.
      Für den Untergrund pro Kanal folgt
      \begin{eqn}
        U_{\t{Poisson}} = \frac{P N_{\t{start}}}{512} ,
      \end{eqn}
      da 512 Kanäle verwendet werden.
      Der berechnete Wert von $U_{\t{Poisson}}$ steht in Tabelle \ref{tab:τ}.
  \section{Diskussion}
    Die lineare Regression der Eichgerade funktionierte gut.
    Der statistische Fehler beim Parameter $B$ ist klein im Vergleich zur Diskretheit der $y$-Achse und bestätigt gut die erwartete Ursprungsgerade.
    Die Ausgleichskurve in Abbildung \ref{fig:τ} beschreibt die Messwerte ebenfalls gut.
    Das Ergebnis für die Lebensdauer weicht um anderthalb Standardabweichungen vom Literaturwert ab, welches ein akzeptables Ergebnis ist.
    Der mittels der Poissonverteilung abgeschätzter Wert für den Untergrund ist höher als der durch die Ausgleichsrechnung gewonnene.
  \makebibliography
\end{document}
